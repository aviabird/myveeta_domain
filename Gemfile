source 'https://rubygems.org'

# Declare your gem's dependencies in myveeta_domain.gemspec.
# Bundler will treat runtime dependencies like base dependencies, and
# development dependencies will be added by default to the :development group.
gemspec

gem 'rake', '10.4.2'
# Declare any dependencies that are still in development here instead of in
# your gemspec. These might include edge Rails or gems from your path or
# Git. Remember to move these dependencies to your gemspec before releasing
# your gem to rubygems.org.

# To use a debugger
# gem 'byebug', group: [:development, :test]
#gem 'annotate'

gem 'acts-as-taggable-on'

# Enumerize attributes with enumerize
gem 'enumerize'

# Don't delete records in db, mark it as deleted with paranoia
gem 'paranoia', '~> 2.0'


# List of countries with ISO3166-1 and currencies ISO4217
gem 'iso_country_codes', github: 'talentsolutions/iso_country_codes'

# List of languages with ISO-639-1
gem 'language_list', '=1.2.0', github: 'talentsolutions/language_list'

# Versioning
gem 'amoeba'

# Use Chewy for easier ElasticSearch
gem 'chewy'

# Use official ElasticSearch Ruby gems for search
gem 'elasticsearch-model'

# Use official ElasticSearch Rails integration
gem 'elasticsearch-rails'

# Track user activity with PublicActivity
gem 'public_activity'

# Extend model validations with date
gem 'date_validator'

# Extend model validations
gem 'activevalidators'

# Upload files with CarrierWave
gem 'carrierwave'

# Use MiniMagick for low-memory image handling
gem 'mini_magick'

# State machine with AASM
gem 'aasm'

# Sequential id in scopes
gem 'sequenced'

#slack notifier
gem 'slack-notifier'

# Authehticate users with Devise
gem 'devise', '~> 3.4.1'

# Extend Devise with easy token authenthication
gem 'simple_token_authentication'

# Extend Devise with advanced token handling for AngularJS
gem 'devise_token_auth', github: 'lynndylanhurley/devise_token_auth', branch: '910596fa5787617e8b64460eb737f3606284a4cf'

# Send devise emails in the background
gem 'devise-async'
