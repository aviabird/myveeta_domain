module JobappsIndex
  extend ActiveSupport::Concern

  included do
    include Elasticsearch::Model

    index_name "veeta-jobapps-#{Rails.env}"
    document_type 'jobapp'

    settings analysis: {
                 filter: {
                     ngram_filter: {
                         type: 'nGram',
                         min_gram: 2,
                         max_gram: 12
                     }
                 },
                 analyzer: {
                     index_ngram_analyzer: {
                         type: 'custom',
                         tokenizer: 'standard',
                         filter: ['lowercase', 'ngram_filter']
                     },
                     search_ngram_analyzer: {
                         type: 'custom',
                         tokenizer: 'standard',
                         filter: ['lowercase']
                     }
                 }
             }

    mappings dynamic: 'true' do
      indexes :talent do
        indexes :first_name, type: 'string', index_analyzer: 'index_ngram_analyzer', search_analyzer: 'search_ngram_analyzer'
        indexes :last_name, type: 'string', index_analyzer: 'index_ngram_analyzer', search_analyzer: 'search_ngram_analyzer'
      end
    end

    def as_indexed_json(options={})
        JSON.parse(
            Jbuilder.encode do |json|
              json.talent do
                json.first_name talent.first_name
                json.last_name talent.last_name
                json.birthdate talent.birthdate
                json.salutation talent.salutation
                json.nationality_country_id talent.nationality_country_id
                json.title talent.title
                json.email talent.email
              end
            end
        )
    end
  end
end
