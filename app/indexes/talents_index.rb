module TalentsIndex
  extend ActiveSupport::Concern

  included do
    include Elasticsearch::Model

    index_name "veeta-talents-#{Rails.env}"
    document_type 'talent'

    settings analysis: {
                 filter: {
                     ngram_filter: {
                         type: 'nGram',
                         min_gram: 2,
                         max_gram: 12
                     }
                 },
                 analyzer: {
                     index_ngram_analyzer: {
                         type: 'custom',
                         tokenizer: 'standard',
                         filter: ['lowercase', 'ngram_filter']
                     },
                     search_ngram_analyzer: {
                         type: 'custom',
                         tokenizer: 'standard',
                         filter: ['lowercase']
                     }
                 }
             }

    mappings dynamic: 'true' do
      indexes :first_name, type: 'string', index_analyzer: 'index_ngram_analyzer', search_analyzer: 'search_ngram_analyzer'
      indexes :last_name, type: 'string', index_analyzer: 'index_ngram_analyzer', search_analyzer: 'search_ngram_analyzer'
      indexes :working_locations, index: 'not_analyzed'
      indexes :work_industries, index: 'not_analyzed'
      indexes :languages, index: 'not_analyzed'
      indexes :skills, index: 'not_analyzed'

    end

    def as_indexed_json(options={})
      JSON.parse(
          Jbuilder.encode do |json|
            json.first_name first_name
            json.last_name last_name
            json.birthdate birthdate
            json.salutation salutation
            json.nationality_country_id nationality_country_id
            json.title title
            json.email email
            json.salary_exp do
              json.min setting.salary_exp_min
              json.max setting.salary_exp_max
            end
            json.work_experience_duration work_experience_duration
            json.working_locations setting.working_locations
            json.work_industries work_industries
            json.languages latest_resume.languages.map(&:iso_code)
            json.skills latest_resume.skills.map(&:name)
          end
      )
    end
  end
end
