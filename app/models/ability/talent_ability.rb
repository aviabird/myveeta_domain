module Ability
  class TalentAbility
    include CanCan::Ability

    def initialize(user)
      # Define abilities for the passed in user here. For example:
      #
      #   user ||= User.new # guest user (not logged in)
      #   if user.admin?
      #     can :manage, :all
      #   else
      #     can :read, :all
      #   end
      #


      cannot :manage, :all

      ###
      # Jobs
      ###
      #anyone can read a job
      can :read, Job

      ###
      # CvSharingRequests
      ###
      #anyone can read a sharing request
      can [:read, :find_by_ext_id], CvSharingRequest

      if user
        ###
        # Talent
        ###
        can :manage, Talent, id: user.id

        ###
        # TalentSetting
        ###
        can :manage, TalentSetting, talent_id: user.id

        ###
        # Resume
        ###
        can :manage, Resume, talent_id: user.id
        can :manage, Resume do |res|
          !res.origin_regardless_deletion.nil? && res.origin_regardless_deletion.talent.auth.talent_id ==  user.id
        end
        can :create, Resume

        ###
        # Jobapps
        ###
        can :manage, Jobapp, resume: { talent_id: user.id }
        can :manage, Jobapp do |jobapp|
          jobapp.resume.nil? || !jobapp.resume.nil? && !jobapp.resume.origin_regardless_deletion.nil? && jobapp.resume.origin_regardless_deletion.talent.auth.talent_id ==  user.id
        end
        can [:new, :create, :validate], Jobapp
        cannot [:update, :finish], Jobapp do |jobapp|
          !jobapp.draft?
        end
        cannot [:finish], Jobapp do |jobapp|
          !user.confirmed?
        end
        #only confirmed user can create jobapp (finishing app)
        # if user.guest? or not user.confirmed?
        # THE ABOVE LINE WAS EXCHANGED FOR THE ONE BELOW FOR VEETA-1693
        # IF THERE ARE ISSUES CONCERNING USER CONFIRMATION, JOBAPP CREATION AND ABILITIES CHECK HERE!!
        if user.guest?
          cannot :create, Jobapp
          can :new, Jobapp
        end

        ###
        # CvSharingResponses
        ###
        can :manage, CvSharingResponse, resume: { talent_id: user.id }

        can :manage, CvSharingResponse do |cvSharingResponse|
          cvSharingResponse.resume.nil? || !cvSharingResponse.resume.nil? && !cvSharingResponse.resume.origin_regardless_deletion.nil? && cvSharingResponse.resume.origin_regardless_deletion.talent.auth.talent_id ==  user.id
        end
        can [:new, :create, :validate], CvSharingResponse
        cannot [:update, :finish], CvSharingResponse do |cvSharingResponse|
          !cvSharingResponse.draft?
        end
        cannot [:finish], CvSharingResponse do |cvSharingResponse|
          !user.confirmed?
        end
        #only confirmed user can create jobapp (finishing app)
        # if user.guest? or not user.confirmed?
        # THE ABOVE LINE WAS EXCHANGED FOR THE ONE BELOW FOR VEETA-1693
        # IF THERE ARE ISSUES CONCERNING USER CONFIRMATION, JOBAPP CREATION AND ABILITIES CHECK HERE!!
        if user.guest?
          cannot :create, CvSharingResponse
          can :new, CvSharingResponse
        end

        ###
        # WorkExperience
        ###
        can :manage, Rs::WorkExperience do |work_experience|
          check_via_resume user, work_experience.resume
        end

        ###
        # Education
        ###
        can :manage, Rs::Education do |education|
          check_via_resume user, education.resume
        end

        ###
        # Skills
        ###
        can :manage, Rs::Skill do |skill|
          check_via_resume user, skill.resume
        end

        ###
        # Certifications
        ###
        can :manage, Rs::Certification do |certification|
          check_via_resume user, certification.resume
        end

        ###
        # Documents
        ###
        can :manage, Rs::Document do |document|
          check_via_resume user, document.resume
        end

        ###
        # Awards
        ###
        can :manage, Rs::Award do |award|
          check_via_resume user, award.resume
        end

        ###
        # Memberships
        ###
        can :manage, Rs::Membership do |membership|
          check_via_resume user, membership.resume
        end

        ###
        # Projects
        ###
        can :manage, Rs::Project do |project|
          check_via_resume user, project.resume
        end

        ###
        # Publications
        ###
        can :manage, Rs::Publication do |publication|
          check_via_resume user, publication.resume
        end

        ###
        # Languages
        ###
        can :manage, Rs::Language do |language|
          check_via_resume user, language.resume
        end

        ###
        # About
        ###
        can :manage, Rs::About do |about|
          check_via_resume user, about.resume
        end

        ###
        # Links
        ###
        can :manage, Rs::Link do |link|
          check_via_resume user, link.resume
        end

        ###
        # About
        ###
        can :manage, PdfTemplateSetting do |pdf_template_setting|
          check_via_resume user, pdf_template_setting.resume
        end

        ###
        # Company
        ###
        can :read, Company do |company|
          ResumeSharing.exists?(company_id: company.id, access_revoked_at: nil, resume_id: Resume.all_versions_of_origins(user.resumes.with_deleted).pluck(:id).to_a)
        end

        ###
        # EmailCompany
        ###
        can :read, EmailApplication do |email_company|
          ResumeSharing.exists?(email_application_id: email_company.id, access_revoked_at: nil, resume_id: Resume.all_versions_of_origins(user.resumes.with_deleted).pluck(:id).to_a)
        end

        ###
        # ClientApplication
        ###
        can :read, ClientApplication do |client_application|
          VeetaButtonSharingConnection.exists?(client_application_id: client_application.id, access_revoked_at: nil, talent_id: user.id)
        end

      end
      # The first argument to `can` is the action you are giving the user
      # permission to do.
      # If you pass :manage it will apply to every action. Other common actions
      # here are :read, :create, :update and :destroy.
      #
      # The second argument is the resource the user can perform the action on.
      # If you pass :all it will apply to every resource. Otherwise pass a Ruby
      # class of the resource.
      #
      # The third argument is an optional hash of conditions to further filter the
      # objects.
      # For example, here the user can only update published articles.
      #
      #   can :update, Article, :published => true
      #
      # See the wiki for details:
      # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
    end

    private
      def check_via_resume user, res
        res.talent_id == user.id || (!res.origin_regardless_deletion.nil? && res.origin_regardless_deletion.talent.auth.talent_id ==  user.id)
      end
  end
end
