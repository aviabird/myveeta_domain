# == Schema Information
#
# Table name: accepted_jobs_languages
#
#  job_id      :uuid
#  language_id :integer
#
# Indexes
#
#  index_accepted_jobs_languages_on_job_id       (job_id)
#  index_accepted_jobs_languages_on_language_id  (language_id)
#

class AcceptedJobsLanguage < ActiveRecord::Base
  belongs_to :job
  belongs_to :language
  include Concerns::HasLanguage
end
