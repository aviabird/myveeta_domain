# == Schema Information
#
# Table name: access_security_token_mappings
#
#  id                   :uuid             not null, primary key
#  individual_key       :string
#  resource_type        :string
#  external_entity_type :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  resource_id          :uuid
#  external_entity_id   :uuid
#  deleted_at           :datetime
#
# Indexes
#
#  index_access_security_token_mappings_on_deleted_at            (deleted_at)
#  index_access_security_token_mappings_on_external_entity_type  (external_entity_type)
#  index_access_security_token_mappings_on_individual_key        (individual_key) UNIQUE
#  index_access_security_token_mappings_on_resource_type         (resource_type)
#

class AccessSecurityTokenMapping < ActiveRecord::Base

  acts_as_paranoid

  validates :individual_key, uniqueness: { case_sensitive: false }

  def unique_part
    resource.unique_part
  end

  def access_security_part
    resource.access_security_part
  end

  def resource
    if resource_type == "Talent"
      Talent.find(resource_id)
    elsif resource_type == "Rs::Document"
      Rs::Document.find(resource_id)
    else
      nil
    end
  end

  def  external_entity
    if external_entity_type == "Company"
      Company.find(external_entity_id)
    elsif external_entity_type == "ClientApplication"
      ClientApplication.find(external_entity_id)
    elsif external_entity_type == "EmailApplication"
      EmailApplication.find(external_entity_id)
    else
      nil
    end
  end

  class << self
    def find_mapping resource, external_entity
      if resource && external_entity
        begin
          AccessSecurityTokenMapping.find_by(resource_type: resource.class.name, resource_id: resource.id, external_entity_type: external_entity.class.name, external_entity_id: external_entity.id)
        rescue
          nil
        end
      end
    end

    def find_or_initialize_mapping resource, external_entity
      mapping = find_mapping resource, external_entity
      unless mapping
          #check uniquness of secure random
          individual_key = SecureRandom.hex(32)
          mapping = AccessSecurityTokenMapping.new(individual_key: individual_key, resource_type: resource.class.name, resource_id: resource.id, external_entity_type: external_entity.class.name, external_entity_id: external_entity.id)
          trials = 0
          until (mapping.valid? || trials == 1000)
            mapping.individual_key = SecureRandom.hex(32)
            trials = trials + 1
          end
          #no unique key found
          unless mapping.valid?
            Rails.logger.fatal("Could not create individual key in 1000 attempts...")
            raise "Could not create individual key in 1000 attempts..."
            return nil
          end
      end
      mapping
    end

    ##################################################################################
    ###### duplicated from above in order to provide Kickresume functionality ########

    def find_or_initialize_kickresume_mapping resource
      mapping = AccessSecurityTokenMapping.find_by(resource_type: resource.class.name, resource_id: resource.id, external_entity_type: "Kickresume")
      unless mapping
          #check uniquness of secure random
          individual_key = SecureRandom.hex(32)
          mapping = AccessSecurityTokenMapping.new(individual_key: individual_key, resource_type: resource.class.name, resource_id: resource.id, external_entity_type: "Kickresume", external_entity_id: nil)
          trials = 0
          until (mapping.valid? || trials == 1000)
            mapping.individual_key = SecureRandom.hex(32)
            trials = trials + 1
          end
          #no unique key found
          unless mapping.valid?
            Rails.logger.fatal("Could not create individual key in 1000 attempts...")
            raise "Could not create individual key in 1000 attempts..."
            return nil
          end
      end
      mapping
    end


    def find_kickresume_mapping resource
      if resource
        begin
          AccessSecurityTokenMapping.find_by(resource_type: resource.class.name, resource_id: resource.id, external_entity_type: "Kickresume", external_entity_id: nil)
        rescue
          nil
        end
      end
    end
  end

end
