# == Schema Information
#
# Table name: admins
#
#  id         :uuid             not null, primary key
#  first_name :string           not null
#  last_name  :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_admins_on_created_at  (created_at)
#

class Admin < ActiveRecord::Base
  include Concerns::UserAbility

  has_one :auth, class_name: 'AdminAuth'
  accepts_nested_attributes_for :auth

  validates :first_name, presence:  true
  validates :last_name, presence:  true
  #validates :auth, presence:  true

  after_initialize do
    self.auth ||= AdminAuth.new if new_record?
  end

  def ability
    @ability ||= Ability::AdminAbility.new(self)
  end

  def name
    "#{first_name} #{last_name}"
  end
end
