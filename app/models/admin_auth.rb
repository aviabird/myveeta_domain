# == Schema Information
#
# Table name: admin_auths
#
#  id                     :uuid             not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  admin_id               :uuid             not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  failed_attempts        :integer          default(0)
#  unlock_token           :string
#  locked_at              :datetime
#  uid                    :string
#
# Indexes
#
#  index_admin_auths_on_created_at            (created_at)
#  index_admin_auths_on_email                 (email) UNIQUE
#  index_admin_auths_on_reset_password_token  (reset_password_token) UNIQUE
#  index_admin_auths_on_unlock_token          (unlock_token) UNIQUE
#

class AdminAuth < ActiveRecord::Base
  include DeviseTokenAuth::Concerns::User
  include Concerns::AuthType

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, #:registerable,:recoverable,
          :rememberable, :trackable, :validatable, :lockable

  belongs_to :admin, inverse_of: :auth

  alias_method :user, :admin

  before_validation :set_uid
  #has_many :client_applications

  #scope :client_applications, ->(client_applications) { where(recruiter: recruiter) }


  #has_many :tokens, -> { order("authorized_at desc").includes(:client_application)}, class_name: "OauthToken"

  def client_applications
    ClientApplication.all
  end

  # DeviceTokenAuth checks for confirmed? regardless of confirmable modle being enabled or not
  # This is a workaround in order to make devise-token-auth working with admin_auths
  def confirmed_at
    true
  end

  private
    def set_uid
      # HACK set uid with user email, this be required by devise-token-auth
      self.uid = self.email
      self.provider = 'email'
    end
end
