# == Schema Information
#
# Table name: application_settings
#
#  id         :integer          not null, primary key
#  setting    :string
#  value      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_application_settings_on_setting  (setting)
#

class ApplicationSetting < ActiveRecord::Base


  class << self

    def timestamp_setting setting
      if exists?(setting: setting)
        app_setting = find_by(setting: setting)
        DateTime.parse(app_setting.value)
      else
        nil
      end
    end

    def set_timestamp_setting setting, value
        app_setting = where(setting: setting).first_or_initialize
        app_setting.value = value.to_s
        app_setting.save
        app_setting
    end
  end
end
