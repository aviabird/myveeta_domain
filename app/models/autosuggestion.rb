# == Schema Information
#
# Table name: autosuggestions
#
#  id         :uuid             not null, primary key
#  key        :string
#  value      :string
#  text       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_autosuggestions_on_created_at  (created_at)
#

class Autosuggestion < ActiveRecord::Base
end
