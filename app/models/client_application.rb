# == Schema Information
#
# Table name: client_applications
#
#  id                            :uuid             not null, primary key
#  name                          :string
#  url                           :string
#  support_url                   :string
#  callback_url                  :string
#  key                           :string(40)
#  secret                        :string(40)
#  user_id                       :uuid
#  created_at                    :datetime
#  updated_at                    :datetime
#  webhook_url                   :string
#  delete_user_profile_deep_link :string
#  logo                          :string
#  cancel_url                    :string
#  use_cancel_callback           :boolean          default(TRUE)
#  contact                       :string
#  deleted_at                    :datetime
#  token                         :string
#  logo_meta                     :text
#
# Indexes
#
#  index_client_applications_on_created_at  (created_at)
#  index_client_applications_on_key         (key) UNIQUE
#  index_client_applications_on_token       (token)
#

require 'oauth'
class ClientApplication < ActiveRecord::Base
  include Concerns::AccessesTalentsResources

  acts_as_paranoid
  belongs_to :user
  has_many :tokens, :class_name => "OauthToken"
  has_many :access_tokens
  has_many :oauth2_verifiers
  has_many :oauth_tokens
  validates_presence_of :name, :url, :key, :secret
  validates_uniqueness_of :key
  before_validation :generate_keys, :on => :create

  validates_format_of :url, :with => /\Ahttp(s?):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/i
  validates_format_of :support_url, :with => /\Ahttp(s?):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/i, :allow_blank=>true
  validates_format_of :callback_url, :with => /\Ahttp(s?):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/i, :allow_blank=>true
  validates_format_of :cancel_url, :with => /\Ahttp(s?):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/i, :allow_blank=>true
  validates_format_of :webhook_url, :with => /\Ahttp(s?):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/i, :allow_blank=>true
  validates_format_of :delete_user_profile_deep_link, :with => /\Ahttp(s?):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/i, :allow_blank=>true
  validates_format_of :contact, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :allow_blank=>false
  validates :logo, presence: true


  attr_accessor :token_callback_url

  mount_uploader :logo, AvatarUploader
  serialize :logo_meta
  attr_accessor :temp_avatar_original_filename

  def temp_avatar_filename=(filename)
    temp_logo = TempUpload::Avatar.find_by_file(filename)
    return if temp_logo.nil?
    self.temp_logo_original_filename = temp_logo.file_meta[:original_filename]
    self.logo = temp_logo.file
  end

  def self.find_token(token_key)
    token = OauthToken.find_by_token(token_key, :include => :client_application)
    if token && token.authorized?
      token
    else
      nil
    end
  end

  def self.verify_request(request, options = {}, &block)
    begin
      signature = OAuth::Signature.build(request, options, &block)
      return false unless OauthNonce.remember(signature.request.nonce, signature.request.timestamp)
      value = signature.verify
      value
    rescue OAuth::Signature::UnknownSignatureMethod => e
      false
    end
  end

  def oauth_server
    @oauth_server ||= OAuth::Server.new("http://your.site")
  end

  def credentials
    @oauth_client ||= OAuth::Consumer.new(key, secret)
  end

  # If your application requires passing in extra parameters handle it here
  def create_request_token(params={})
    RequestToken.create :client_application => self, :callback_url=>self.token_callback_url
  end

  # Dummy link to logo
  def logo_url
    {
      thumb: logo.thumb.url,
      small:  logo.small.url,
      medium: logo.medium.url,
      large:  logo.large.url,
      orginal: logo.url
    } if logo?
  end

protected

  def generate_keys
    self.key = OAuth::Helper.generate_key(40)[0,40]
    self.secret = OAuth::Helper.generate_key(40)[0,40]
  end
end
