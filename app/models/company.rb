# == Schema Information
#
# Table name: companies
#
#  id                      :uuid             not null, primary key
#  owner_id                :uuid
#  name                    :string           not null
#  tnc_text                :text
#  tnc_link                :string
#  veeta_terms_accepted_at :datetime         not null
#  veeta_terms_accepted_ip :string           not null
#  veeta_terms_accepted_by :string           not null
#  plan                    :string
#  deleted_at              :datetime
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  contact                 :text
#  logo                    :string
#  blocking_notice         :boolean          default(TRUE)
#  token                   :string
#  logo_meta               :text
#
# Indexes
#
#  index_companies_on_created_at  (created_at)
#  index_companies_on_deleted_at  (deleted_at)
#  index_companies_on_name        (name)
#  index_companies_on_owner_id    (owner_id)
#  index_companies_on_token       (token)
#

class Company < ActiveRecord::Base
  include Concerns::AccessesTalentsResources

  ## modules
  include AASM
  acts_as_paranoid

  ## associations
  has_many :recruiters, inverse_of: :company
  belongs_to :owner, class_name: 'Recruiter'
  has_one :setting, class_name: 'CompanySetting'
  has_many :talent_infos, class_name: 'CompanyTalentInfo'
  has_many :talents, through: :talent_infos
  has_many :recruiters
  has_many :jobs
  has_many :recruiter_comments
  has_many :pools
  has_many :sharings, class_name: 'ResumeSharing'
  has_many :resumes, through: :sharings

  mount_uploader :logo, AvatarUploader
  serialize :logo_meta
  attr_accessor :temp_avatar_original_filename

  # callbacks
  after_create :provision_unsolicited_job

  rails_admin do
     exclude_fields :token
  end if defined?(RailsAdmin)

  def provision_unsolicited_job
    job = Job.new
    job.unsolicited_job = true
    # unsolicited jobs have no name
    job.company = self 
    job.status = 'new'
    job.recruiter = self.owner
    job.language = Language.find('de')
    job.accepted_jobs_languages.build(language: Language.find('en'))
    job.accepted_jobs_languages.build(language: Language.find('de'))
    job.save
  end

  def temp_avatar_filename=(filename)
    temp_logo = TempUpload::Avatar.find_by_file(filename)
    return if temp_logo.nil?
    self.temp_logo_original_filename = temp_logo.file_meta[:original_filename]
    self.logo = temp_logo.file
  end

  ## validations
  validates :name, :owner, presence: true

  validates :short_name, uniqueness: true,length: { is: 3 }

  scope :in_touch, -> (talent) { ResumeSharing.where(resume_id: talent.resumes_version_ids, access_revoked_at: nil).map(&:company).uniq }
  #scope :talents_via_sharing, -> { ResumeSharing.where(company: self, access_revoked_at: nil).map(&:resume).uniq }


  ## state machine
  aasm column: :plan do
    state :basic, initial: true
    state :pro
    state :ent
  end

  alias_method :recruiter, :owner

  # Dummy link to logo
  def logo_url
    {
      thumb:  logo.thumb.url,
      small:  logo.small.url,
      medium: logo.medium.url,
      large:  logo.large.url,
      orginal:  logo.url
    } if logo?
  end

  def accept_terms(email, ip)
    self.veeta_terms_accepted_at = DateTime.now.to_s(:db)
    self.veeta_terms_accepted_by = email
    self.veeta_terms_accepted_ip = ip
  end

  # get most recent sharing
  def unique_sharings
    sharing_per_talent = {}
    sharings.each do |sharing|
      origin_id = sharing.resume.origin_id
      # get talent id via origin resume
      talent_id = Resume.with_deleted.find(origin_id) && Resume.with_deleted.find(origin_id).talent ? Resume.with_deleted.find(origin_id).talent.id : nil
      if talent_id && sharing_per_talent[talent_id] && sharing_per_talent[talent_id].updated_at < sharing.updated_at
        #more recent sharing found for talent
        sharing_per_talent[talent_id] = sharing
      elsif talent_id && sharing_per_talent[talent_id].blank?
        #first sharing entry for talent found
        sharing_per_talent[talent_id] = sharing
      end
    end
    ResumeSharing.where("id in (?)",sharing_per_talent.values.map(&:id))
  end

  # get unique resume_sharings flagged with access_revoked_at
  def unique_revoked_sharings
    sharing_per_talent = {}
    sharing_ids = []
    sharings.where.not(access_revoked_at: nil).order(:access_revoked_at).each do |sharing|
      origin_id = sharing.resume.origin_id
      # get talent id via origin resume
      talent_id = Resume.with_deleted.find(origin_id) && Resume.with_deleted.find(origin_id).talent ? Resume.with_deleted.find(origin_id).talent.id : nil
      # check if access_revoked_at of sharing is the same (+/- 2 seconds threshold) as for the previous entry of the talent
      if talent_id && sharing_per_talent[talent_id] && sharing.access_revoked_at.between?(sharing_per_talent[talent_id].access_revoked_at-2.seconds,sharing_per_talent[talent_id].access_revoked_at+2.seconds)
        #more recent sharing found for talent
        sharing_per_talent[talent_id] = sharing
        #same
      elsif talent_id && (sharing_per_talent[talent_id].blank? || sharing.access_revoked_at.between?(sharing_per_talent[talent_id].access_revoked_at-2.seconds,sharing_per_talent[talent_id].access_revoked_at+2.seconds))
        #first sharing entry for talent found
        sharing_per_talent[talent_id] = sharing
        sharing_ids << sharing.id
      end
    end
    ResumeSharing.where("id in (?)",sharing_ids)
  end

  # get most recent sharing before the current update release cycle - or the very first sharing
  def unique_sharings_before_update_release_cycle update_timestamp
    sharing_per_talent = {}
    sharings.order(:updated_at).each do |sharing|
      origin_id = sharing.resume.origin_id
      # get talent id via origin resume
      talent_id = Resume.with_deleted.find(origin_id) && Resume.with_deleted.find(origin_id).talent ? Resume.with_deleted.find(origin_id).talent.id : nil
      # if an initial sharing already exists
      next if !sharing_per_talent[talent_id].nil? && sharing.updated_at > update_timestamp
      if talent_id && sharing_per_talent[talent_id] && sharing_per_talent[talent_id].updated_at < sharing.updated_at
        #more recent sharing found for talent
        sharing_per_talent[talent_id] = sharing
      elsif talent_id && sharing_per_talent[talent_id].blank?
        #first sharing entry for talent found
        sharing_per_talent[talent_id] = sharing
      end
    end
    ResumeSharing.where("id in (?)",sharing_per_talent.values.map(&:id))
  end
end
