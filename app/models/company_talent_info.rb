# == Schema Information
#
# Table name: company_talent_infos
#
#  id         :integer          not null, primary key
#  company_id :uuid
#  talent_id  :uuid
#  rating     :integer
#  note       :text
#  category   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_company_talent_infos_on_company_id  (company_id)
#  index_company_talent_infos_on_talent_id   (talent_id)
#

class CompanyTalentInfo < ActiveRecord::Base
  belongs_to :company
  belongs_to :talent

  acts_as_sequenced scope: :company_id

  validates :company, :talent, presence: true

  before_create :set_hashed_talent_id

  private

  def set_hashed_talent_id
    begin
      self.hashed_talent_id_salt = generate_code(16)
      self.hashed_talent_id =  OpenSSL::HMAC.hexdigest('sha256', "#{self.talent_id}+#{self.company_id}", self.hashed_talent_id_salt)
    end
  end

  def generate_code(number)
    charset = Array('A'..'Z') + Array('a'..'z') + Array('0'..'9') + ['%','&','$','!','-','_','@']
    Array.new(number) { charset.sample }.join
  end

end
