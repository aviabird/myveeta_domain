module Concerns
  module Abilited
    extend ActiveSupport::Concern

    # Constantize current class
    def class_constantized
      self.class.name.constantize
    end

    # Get list of things, whitch current user can do with this object
    def can current_user
      abilities.select{|a| current_user.can?(a, self) } if current_user
    end

    # Get list of abilities specified in each class
    def abilities
      defined?(class_constantized::ABILITIES) ? class_constantized::ABILITIES : [:create, :read, :update, :delete]
    end
  end
end