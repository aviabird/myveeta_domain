module Concerns

  # objects which are accessed by external actors (e.g. documents and talent (avatar) accessed by company)
  module AccessedByExternal

    #access security part of token
    def access_security_part
      if token
        token[41, 8]
      else
        #if no token set, fake a random token
        SecureRandom.urlsafe_base64(8)
      end
    end

    def unique_part
      if token
        token[0, 41]
      else
        #if no token set, fake a random token
        SecureRandom.urlsafe_base64(41)
      end
    end

    def parpare_external_path path, secure_access_token
      path_parts = path.split '/'
      path_parts[path_parts.size-2] = secure_access_token
      "/#{path_parts.join('/')}"
    end

    def resource_file_available?
      !token.blank?
    end

  end
end
