module Concerns

  # actors which access "privately shared" data (e.g. company accesses documents and talent (avatar))
  module AccessesTalentsResources

    # individual part of resource path for each company or third party
    def access_path_token resource
      mapping = AccessSecurityTokenMapping.find_mapping resource, self
      if mapping
        mapping.individual_key
      else
        trials = 0
        individual_key = SecureRandom.hex(32)
        while (AccessSecurityTokenMapping.exists?(individual_key: individual_key) && trials < 1000)
          individual_key = SecureRandom.hex(32)
          trials = trials + 1
        end
        if AccessSecurityTokenMapping.exists?(individual_key: individual_key)
          Rails.logger.fatal("Could not create individual key in 1000 attempts...")
          raise "Could not create individual key in 1000 attempts..."
          return nil
        else
          individual_key
        end
      end
    end
  end
end
