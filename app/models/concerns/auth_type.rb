module Concerns
  module AuthType
    extend ActiveSupport::Concern

    included do
      delegate :talent?, :recruiter?, :admin?, :can?, :cannot?, to: :user

      validates :email, presence: true
    end

    def expire_sessions!
      update_column(:tokens, '{}')
    end
  end
end
