module Concerns
  module Compatible
    extend ActiveSupport::Concern

    def method_missing(method, *args, &block)
      if method.to_s =~ /^is_(.+)$/
        self.send("#{method.to_s.gsub('is_', '')}?")
      else
        super # You *must* call super if you don't handle the
              # method, otherwise you'll mess up Ruby's method
              # lookup.
      end
    end
  end
end