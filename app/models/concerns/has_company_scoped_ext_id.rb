module Concerns
  module HasCompanyScopedExtId
    extend ActiveSupport::Concern

    # imitates before_save :ensure_ext_id
    def self.included(base)
      base.class_eval do
        before_save :ensure_ext_id
        before_validation :add_company_short_name, on: :create
        before_validation :update_company_short_name, on: :update
      end
    end

    def ensure_ext_id
      if ext_id.blank?
        self.ext_id = generate_ext_id
      end
    end

    # remove company short name part from job id
    def unscoped_ext_id
      unless ext_id.blank? || !ext_id.include?('-')
        self.ext_id.split('-').drop(1).join('-')
      else
        self.ext_id
      end
    end

    def add_company_short_name
      if !self.ext_id.blank? && !self.ext_id.starts_with?("#{company.short_name}-")
        self.ext_id = "#{company.short_name}-#{self.ext_id}"
      end
    end

    def update_company_short_name
      if self.changes[:ext_id]
        # does not start with current short name
        if !self.ext_id.blank? && !self.ext_id.starts_with?("#{company.short_name}-")
          self.ext_id = "#{company.short_name}-#{self.ext_id}"
        end
        # do nothing: starts with different shortname
        # do nothing: starts with current short name
      end
    end

    def generate_ext_id
      loop do
        ext_id = "#{company.short_name}-#{Devise.friendly_token.slice(0,5)}"
        break ext_id unless self.class.find_by(ext_id: ext_id)
      end
    end
  end
end
