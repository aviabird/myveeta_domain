module Concerns
  module HasCountry
    extend ActiveSupport::Concern

    def country
      Country.find(country_id)
    end

    def country= country
      self.country_id = country.try(:id)
    end
  end
end
