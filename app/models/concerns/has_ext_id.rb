module Concerns
  module HasExtId
    extend ActiveSupport::Concern

    # imitates before_save :ensure_ext_id
    def self.included(base)
      base.class_eval do
        before_save :ensure_ext_id
      end
    end

    def ensure_ext_id
      if ext_id.blank?
        self.ext_id = generate_ext_id
      end
    end

    def generate_ext_id
      loop do
        # from Devise.friendly_token
        ext_id = SecureRandom.urlsafe_base64(15).tr('lIO0', 'sxyz').slice(0,5)
        break ext_id unless self.class.find_by(ext_id: ext_id)
      end
    end
  end
end
