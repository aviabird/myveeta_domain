module Concerns
  module HasLanguage
    extend ActiveSupport::Concern

    def language
      Language.find(language_id)
    end

    def language= language
      self.language_id = language.try(:id)
    end
  end
end
