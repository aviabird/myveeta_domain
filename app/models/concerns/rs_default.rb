module Concerns
  module RsDefault
    extend ActiveSupport::Concern

    included do
      after_save :update_resume
      after_destroy :update_resume
    end

    private
      def update_resume
        #only update if changes to resume were made
        latest_version = resume.origin.nil? ? resume.latest : resume
        if !CloningService::ResumeComparison.call(resume,latest_version)
          resume.update_column(:updated_at, DateTime.now)
        end

      end
  end
end
