# Read more: https://github.com/talentsolutions/iso_country_codes

class Country
  include ActiveModel::Model
  include ActiveModel::Serialization

  delegate :id, :name, :numeric, :alpha2, :alpha3, :calling,
    :continent, :currency, to: :@origin

  def initialize origin
    raise ArgumentError, 'origin cant be nil' if origin == nil
    @origin = origin
  end

  alias_method :id, :numeric
  alias_method :iso_code, :alpha2

  def attributes
    {
      'id' => nil,
      'name' => nil,
      'iso_code' => nil
    }
  end

  def name
    "iso_country_codes_country_names_#{alpha2}"
  end

  def marked_for_destruction?
    false
  end

  # TODO forward all missing methods to IsoCountryCodes
  class << self
    def find id = nil
      if id
        # Append zeroes to keep format in 001
        id = ("%03d" % id) if id.is_a?(Integer)
        begin
          new IsoCountryCodes.find(id)
        rescue IsoCountryCodes::UnknownCodeError
          nil
        end
      end
    end

    def all
      IsoCountryCodes.all.map{|c| new(c)}
    end
  end
end
