# == Schema Information
#
# Table name: custom_autosuggestions
#
#  id           :uuid             not null, primary key
#  key          :string
#  scope        :string
#  value        :string
#  text         :string
#  talent_id    :uuid
#  recruiter_id :uuid
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_custom_autosuggestions_on_created_at  (created_at)
#

class CustomAutosuggestion < ActiveRecord::Base
  belongs_to :talent
  belongs_to :recruiter
end
