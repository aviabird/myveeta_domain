# == Schema Information
#
# Table name: cv_sharing_requests
#
#  id                :uuid             not null, primary key
#  company_id        :uuid             not null
#  recruiter_id      :uuid
#  pool_id           :uuid
#  language_id       :integer          not null
#  status            :string
#  blocking_notice   :boolean
#  contact_details   :text
#  ext_id            :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  pool_welcome_text :string
#
# Indexes
#
#  index_cv_sharing_requests_on_company_id   (company_id)
#  index_cv_sharing_requests_on_ext_id       (ext_id)
#  index_cv_sharing_requests_on_language_id  (language_id)
#  index_cv_sharing_requests_on_pool_id      (pool_id)
#

class CvSharingRequest < ActiveRecord::Base
  include Concerns::HasCompanyScopedExtId

  belongs_to :company
  belongs_to :recruiter
  belongs_to :pool
  #belongs_to :language
  include Concerns::HasLanguage
  has_many :cv_sharing_responses, as: :cv_sharing_request

  validates :company, :language, presence: true

  validates :ext_id, uniqueness: true

  class << self
    def find_all_active_or_initialize recruiter
      cv_sharing_requests = CvSharingRequest.where(company: recruiter.company, status: 'new')
      if cv_sharing_requests.empty?
        CvSharingRequest.create(company: recruiter.company, language: Language.find('de'), status: 'new', blocking_notice: 'f')
        cv_sharing_requests = where(company: recruiter.company, status: 'new')
      end
      cv_sharing_requests
    end
  end
end
