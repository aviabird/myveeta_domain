# == Schema Information
#
# Table name: cv_sharing_responses
#
#  id                              :uuid             not null, primary key
#  resume_id                       :uuid
#  cv_sharing_request_id           :uuid             not null
#  cv_sharing_request_type         :string           not null
#  blocked_companies               :text
#  read_at                         :datetime
#  draft                           :boolean          default(TRUE)
#  application_referrer_url        :string
#  allow_recruiter_contact_sharing :boolean          default(FALSE), not null
#  company_terms_accepted_at       :datetime
#  company_terms_accepted_ip       :string
#  access_revoked_at               :datetime
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#  update_only                     :boolean          default(FALSE)
#
# Indexes
#
#  index_cv_sharing_responses_on_access_revoked_at      (access_revoked_at)
#  index_cv_sharing_responses_on_cv_sharing_request_id  (cv_sharing_request_id)
#  index_cv_sharing_responses_on_read_at                (read_at)
#  index_cv_sharing_responses_on_resume_id              (resume_id)
#

class CvSharingResponse < ActiveRecord::Base
  belongs_to :resume
  has_one :origin_with_deleted, through: :resume, source: :origin_with_deleted
  has_one :talent, through: :resume
  belongs_to :cv_sharing_request, polymorphic: true

  validates :cv_sharing_request, presence: true

  serialize :blocked_companies, Array

  belongs_to :real_cv_sharing_request, -> { where(cv_sharing_responses: {cv_sharing_request_type: 'CvSharingRequest'}) },  class_name: 'CvSharingRequest', foreign_key: 'cv_sharing_request_id'

  # Determinate to which company is application applied
  def respond_to
    cv_sharing_request.try(:company)
  end

  alias_method :company, :respond_to

  def currently_shared_cv talent
    #talent.shares_with?(cv_sharing_request.company.kind_of?(Company) ? "REAL" : "EMAIL" , cv_sharing_request.company.id)
    talent.currently_shared_cv(cv_sharing_request.company.kind_of?(Company) ? "REAL" : "EMAIL" , cv_sharing_request.company.id)
  end

  def my_type
    'CvSharingResponse'
  end

  alias_method :job_type, :my_type


  class << self

    def my_cv_sharing_requests talent
      all_versions_ids = talent.versioned_resumes_ids
      CvSharingResponse.where(resume_id: all_versions_ids, update_only: false, draft: false)
    end

    def my_most_recent_sharing_response_at_company talent, company_id
      all_versions_ids = talent.versioned_resumes_ids
      CvSharingResponse.joins(:real_cv_sharing_request).where(cv_sharing_requests: {company_id: company_id},resume_id: all_versions_ids).where(access_revoked_at: nil).order('created_at desc').limit(1)
    end

    # Skip validations for talent when preparing new resume
    def prepare params = {}
      if params[:cv_sharing_request_id]
        prepare_sharing_response(params[:cv_sharing_request_id], params[:application_referrer_url])
      else
        prepare_sharing_email
      end
    end

    def prepare_sharing_email
    # prepare cv_sharing_email
    #  new(job: EmailApplication.new)
    end

    def prepare_sharing_response cv_sharing_request_id, application_referrer_url
      create(cv_sharing_request_id: cv_sharing_request_id, application_referrer_url: application_referrer_url, cv_sharing_request_type: 'CvSharingRequest')
    end

    def initial_responses_per_company company_id, update_since
      find_by_sql(["select distinct on (r2.talent_id) c1.* from cv_sharing_responses c1
        join cv_sharing_requests c2 on c1.cv_sharing_request_id =c2.id
        join resumes r1 on r1.id = c1.resume_id
        join resumes r2 on r2.id= r1.origin_id
        where company_id=(?) and draft='f' and update_only='f' and submitted_at > (?)
        order by r2.talent_id, c1.submitted_at", company_id,update_since])
    end
  end
end
