module DeviseTokenAuthLockableFix
  # This is a fix for using the lockable module of devise togehter with
  # Devise Token Auth. Without this, failed logins would not be counted
  # when logging in with DTA
  def valid_for_authentication?
    super { false }
  end
end
