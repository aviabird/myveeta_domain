# == Schema Information
#
# Table name: email_applications
#
#  id              :uuid             not null, primary key
#  company_id      :uuid
#  recruiter_id    :uuid
#  company_name    :string
#  jobname         :string
#  recruiter_email :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  draft           :boolean          default(TRUE)
#  unsolicited     :boolean
#
# Indexes
#
#  index_email_applications_on_company_id       (company_id)
#  index_email_applications_on_created_at       (created_at)
#  index_email_applications_on_recruiter_email  (recruiter_email)
#  index_email_applications_on_recruiter_id     (recruiter_id)
#

class EmailApplication < ActiveRecord::Base
  include Concerns::AccessesTalentsResources

  has_one :jobapp, as: :job
  belongs_to :company
  belongs_to :recruiter
  has_many :sharings, class_name: 'ResumeSharing'

  validates :jobname, presence: true, unless: :unsolicited
  validates :recruiter_email, presence: true

  #fake company, required for serializing companies of a resume
  def company
    Company.new(name: company_name, contact: recruiter_email, blocking_notice:false)
  end

  def name
    jobname
  end

end
