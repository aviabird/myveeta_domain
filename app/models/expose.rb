# == Schema Information
#
# Table name: exposes
#
#  id         :uuid             not null, primary key
#  jobapp_id  :uuid             not null
#  content    :text
#  subject    :string           not null
#  deleted_at :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_exposes_on_created_at  (created_at)
#  index_exposes_on_deleted_at  (deleted_at)
#  index_exposes_on_jobapp_id   (jobapp_id)
#

class Expose < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :jobapp

  validates_associated :jobapp
  validates_presence_of :jobapp_id,
                        :subject
end
