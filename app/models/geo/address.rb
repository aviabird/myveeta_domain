# == Schema Information
#
# Table name: geo_addresses
#
#  id             :uuid             not null, primary key
#  country_id     :integer
#  state_id       :uuid
#  zip            :string
#  city           :string
#  address_line   :string
#  address_line_2 :string
#  latitude       :float
#  longitude      :float
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_geo_addresses_on_address_line    (address_line)
#  index_geo_addresses_on_address_line_2  (address_line_2)
#  index_geo_addresses_on_city            (city)
#  index_geo_addresses_on_country_id      (country_id)
#  index_geo_addresses_on_created_at      (created_at)
#  index_geo_addresses_on_latitude        (latitude)
#  index_geo_addresses_on_longitude       (longitude)
#  index_geo_addresses_on_state_id        (state_id)
#  index_geo_addresses_on_zip             (zip)
#

module Geo
  class Address < ActiveRecord::Base
    belongs_to :country
    include Concerns::HasCountry

    belongs_to :state, class_name: 'Geo::State'

    validates :country, presence: true
    validates :zip, presence: true
    validates :city, presence: true
    validates :address_line, presence: true

    amoeba do
      enable
    end

    def address_empty?
      country.blank? && zip.blank? && city.blank? && address_line.blank? && state.nil?
    end

  end
end
