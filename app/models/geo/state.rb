# == Schema Information
#
# Table name: geo_states
#
#  id         :uuid             not null, primary key
#  country_id :integer          not null
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_geo_states_on_country_id  (country_id)
#  index_geo_states_on_created_at  (created_at)
#  index_geo_states_on_name        (name)
#

module Geo
  class State < ActiveRecord::Base
    belongs_to :country
    include Concerns::HasCountry

    validates :country, presence: true
    validates :name, presence: true
  end
end
