# == Schema Information
#
# Table name: jobs
#
#  id                    :uuid             not null, primary key
#  company_id            :uuid             not null
#  recruiter_id          :uuid
#  assigned_recruiter_id :uuid
#  language_id           :integer          not null
#  status                :string
#  name                  :string           not null
#  code                  :string
#  reference             :string
#  contact_details       :text
#  deleted_at            :datetime
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  ext_id                :string
#
# Indexes
#
#  index_jobs_on_company_id    (company_id)
#  index_jobs_on_created_at    (created_at)
#  index_jobs_on_ext_id        (ext_id)
#  index_jobs_on_name          (name)
#  index_jobs_on_recruiter_id  (recruiter_id)
#  index_jobs_on_status        (status)
#

class Job < ActiveRecord::Base
  include Concerns::HasCompanyScopedExtId

  acts_as_taggable

  belongs_to :company
  belongs_to :recruiter
  belongs_to :assigned_recruiter, class_name: 'Recruiter'
  belongs_to :language
  include Concerns::HasLanguage
  has_many :jobapps, as: :job
  has_many :accepted_jobs_languages, -> { uniq }
  has_many :languages, through: :accepted_jobs_languages

  validates :company, :language, :accepted_jobs_languages, presence: true
  validates :name, presence: true, unless: :unsolicited_job?

  validates :ext_id, uniqueness: true

  scope :maintained_by, ->(recruiter) { where(recruiter: recruiter) }

  # alias_method :accepted_jobs_languages, :geo_languages
  # Check if job is closed or not
  def closed?
    status == 'closed'
  end

  # Check if is job deleted or not
  def deleted?
    deleted_at != nil
  end

  # Check if is job still available or not
  def available?
    !closed? && !deleted?
  end

  # Get the company name directly
  def company_name
    company.name
  end

  def jobapps_count
    jobapps.count
  end

  def disable
    update_column(:status, 'closed')
  end

end
