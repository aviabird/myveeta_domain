class JobExpectation
  include ActiveModel::Serialization

  def initialize user
    @user = user
  end

  delegate :setting, to: :@user
  delegate :update, :update!, to: :setting
  delegate :job_seeker_status, :availability, :salary_exp_min, :salary_exp_max, :working_industries, :working_locations, :prefered_employment_type, to: :setting

  def attributes
    {
      job_seeker_status: job_seeker_status,
      availability: availability,
      salary_exp_min: salary_exp_min,
      salary_exp_max: salary_exp_max,
      working_industries: working_industries,
      working_locations: working_locations,
      prefered_employment_type: prefered_employment_type
    }
  end
end
