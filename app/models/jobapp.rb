# == Schema Information
#
# Table name: jobapps
#
#  id                              :uuid             not null, primary key
#  resume_id                       :uuid
#  job_id                          :uuid             not null
#  job_type                        :string           not null
#  language_id                     :integer
#  cover_letter                    :text
#  contact_preferences             :text
#  blocked_companies               :text
#  source                          :string
#  recruiter_rating                :integer
#  recruiter_note                  :text
#  talent_info_status              :string
#  added_by_recruiter              :boolean          default(FALSE), not null
#  read_at                         :datetime
#  resume_snapshot_id              :uuid
#  application_referrer_url        :string
#  allow_recruiter_contact_sharing :boolean          default(FALSE), not null
#  company_terms_accepted_at       :datetime
#  company_terms_accepted_ip       :string
#  withdrawn_at                    :datetime
#  withdrawn_reason                :text
#  access_revoked_at               :datetime
#  hidden_at                       :datetime
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#  draft                           :boolean          default(TRUE)
#  source_details                  :text
#  revoke_reason                   :string
#  submitted_at                    :datetime
#
# Indexes
#
#  index_jobapps_on_access_revoked_at   (access_revoked_at)
#  index_jobapps_on_added_by_recruiter  (added_by_recruiter)
#  index_jobapps_on_created_at          (created_at)
#  index_jobapps_on_job_id              (job_id)
#  index_jobapps_on_job_type            (job_type)
#  index_jobapps_on_language_id         (language_id)
#  index_jobapps_on_read_at             (read_at)
#  index_jobapps_on_recruiter_rating    (recruiter_rating)
#  index_jobapps_on_resume_id           (resume_id)
#  index_jobapps_on_talent_info_status  (talent_info_status)
#

class Jobapp < ActiveRecord::Base

  ## modules
  extend Enumerize
  include JobappsIndex


  ## settings
  enumerize :source, in: [:company_website, :job_agency, :online_jobplatform,
                          :personal_recommendation, :printed_media,
                          :social_media, :other_source]

  serialize :blocked_companies, Array
  attr_accessor :terms
  include AASM

  belongs_to :resume
  has_one :origin, through: :resume, source: :origin

  has_one :talent, through: :resume

  belongs_to :job, polymorphic: true
  accepts_nested_attributes_for :job

  belongs_to :real_job, -> { where(jobapps: {job_type: 'Job'}) },  class_name: 'Job', foreign_key: 'job_id'
  belongs_to :email_application, -> { where(jobapps: {job_type: 'EmailApplication'}) }, foreign_key: 'job_id'

  has_one :expose
  belongs_to :language
  include Concerns::HasLanguage

  # validates :resume_id, presence: true
  validates :job_id, presence: true, if: :is_job
  validates :terms, acceptance: true
  # validate :matching_languages

  has_many :recruiter_comments, as: :commented

  scope :nonrevoked, -> { where(access_revoked_at: nil) }
  scope :dummy, -> { joins(:resume).where(resumes: {id: 1}) }

  aasm column: :talent_info_status do
    state :received, initial: true
    state :review
    state :declined
  end

  after_create :create_connect

  # Determinate to which company is application applied
  def applied_to
    job.try(:company)
  end

  alias_method :company, :applied_to

  # Determinate which talent created the jobapp
  def applied_by
    resume.try(:talent)
  end

  alias_method :talent, :applied_by

  # Try to get someone, who is responsible for current jobapp
  def responsible_person
    job.try(:recruiter)
  end

  alias_method :recruiter, :responsible_person

  # Figer out if job is available
  def available?
    job.try(:available?)
  end

  # Revoke access by flaging as revoked
  def revoke
    self.access_revoked_at = DateTime.now
    save
  end

  # Check if possible to create new jobapp or view old one
  def check
    self.errors.add(:job, "is no longer available") if job.kind_of?(Job) and !job.available?
  end

  def attributes=(attributes = {})
    self.job_type = attributes[:job_type]
    super
  end

  def job_attributes=(attributes)
    if job_type == 'EmailApplication' # or job_type == 'Job'
      #byebug
      atts = job_attributes_whitelisted(attributes, job_type)
      if !self.job
        self.job = job_type.constantize.new(
          company_name:    atts[:company_name],
          jobname:         atts[:jobname],
          recruiter_email: atts[:recruiter_email],
          unsolicited:     atts[:unsolicited]
        )
      else
        self.job.update(job_attributes_whitelisted(attributes, job_type))
      end

    end
  end

  # Permit only given attributes by type of job
  def job_attributes_whitelisted attributes, job_type
    if job_type == 'EmailApplication'
      attributes.slice(:company_name, :jobname, :recruiter_email, :unsolicited)
    else
      {}
    end
  end

  # Withdrawn jobapp
  def withdrawn reason
    if withdrawn_at == nil
      self.withdrawn_at = DateTime.now
      self.withdrawn_reason = reason
      save
      Resque.enqueue_in(ENV['INSTANT_MAIL_DELAY'].to_i, MailWithdrawJobappJob, I18n.locale, self.id)
    else
      self.errors.add(:withdrawn_at, "has been alrady set")
    end
  end

  def is_job
    job_type != 'EmailApplication'
  end

  # # Assign company directly
  # def company= company
  #   job.update(company: company)
  # end

  def hidden?
    hidden_at?
  end

  def origin_resume
    resume.origin if resume
  end

  def is_talents_first_jobapp?
    origin = Resume.with_deleted.find(resume.origin_id)
    Jobapp.where(resume_id: origin.origins_resumes.pluck(:id)).count == 1
  end

  private
    def set_language
      self.language = resume.language if resume
    end

    # Validate matching languages of Resume and Job (if is not EmailApplication)
    def matching_languages
      set_language
      if job_type == 'Job' || job_type == Job
        errors.add(:language, "of your resume is different") unless job.accepted_jobs_languages.include?(language)
      end
    end

    def create_connect
      CompanyTalentInfo.find_or_create_by(company: company, talent: talent) if company
    end

  class << self

    def my_jobapps talent_id
      origin_ids = Resume.with_deleted.select(:id).where(talent_id: talent_id).to_a
      all_versions_ids = Resume.select(:id).where("origin_id in (?)",origin_ids).to_a
      Jobapp.where(resume_id: all_versions_ids)
    end

    def my_most_recent_jobapp_at_company talent_id, company_id
      origin_ids = Resume.with_deleted.select(:id).where(talent_id: talent_id).to_a
      all_versions_ids = Resume.select(:id).where("origin_id in (?)",origin_ids).to_a
      Jobapp.joins(:real_job).where(jobs: {company_id: company_id},resume_id: all_versions_ids).where(access_revoked_at: nil).order('created_at desc').limit(1)
    end

    # Skip validations for talent when preparing new resume
    def prepare params = {}
      if params[:job_id]
        prepare_job_application(params[:job_id], params[:application_referrer_url])
      else
        prepare_email_application
      end
    end

    def prepare_email_application
      new(job: EmailApplication.new)
    end

    def prepare_job_application job_id, application_referrer_url
      create(job_id: job_id, job_type: 'Job', application_referrer_url: application_referrer_url)
    end

    def my_or_dummy talent
      talent.jobapps.empty? ? dummy : my(talent)
    end
  end
end
