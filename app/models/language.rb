# Read more: https://github.com/scsmith/language_list

class Language
  include ActiveModel::Model
  include ActiveModel::Serialization

  delegate :name, :iso_639_1, :iso_639_3, :common, to: :@origin

  def initialize origin
    raise ArgumentError, 'origin cant be nil' if origin == nil
    @origin = origin
  end

  alias_method :iso_code, :iso_639_1

  def id
    iso_code.split('').map{|b| "%03d" % b.ord }.join
  end

  def attributes
    {
      'id' => nil,
      'name' => nil,
      'iso_code' => nil
    }
  end

  def marked_for_destruction?
    false
  end

  def hello
    Hello.by_language(self)
  end

  class << self
    def find id = nil
      if id
        id = id_to_iso(id) unless id.to_s.to_i == 0
        begin
          new LanguageList::LanguageInfo.find(id.to_s)
        rescue ArgumentError
          nil
        end
      end
    end

    def all
      LanguageList::COMMON_LANGUAGES.map{|l| new(l)}
    end

    private
      def id_to_iso id
        ("%06d" % id.to_i)[0,3].to_i.chr + ("%06d" % id.to_i)[3,6].to_i.chr
      end
  end
end
