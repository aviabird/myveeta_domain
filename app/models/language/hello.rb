class Language::Hello
  HELLO = {}
  HELLO['ar'] = 'مرحبا'
  HELLO['bg'] = 'Здравейте'
  HELLO['ca'] = 'Hola'
  HELLO['zh'] = '你好'
  HELLO['cs'] = 'Ahoj'
  HELLO['da'] = 'Hej'
  HELLO['nl'] = 'Hallo'
  HELLO['en'] = 'hello'
  HELLO['et'] = 'Tere'
  HELLO['fi'] = 'Moi'
  HELLO['fr'] = 'Salut'
  HELLO['de'] = 'Hallo'
  HELLO['el'] = 'Γεια σου'
  HELLO['ht'] = 'Creole Bonjou'
  HELLO['he'] = 'שלום'
  HELLO['hi'] = 'नमस्कार'
  HELLO['hu'] = 'helló'
  HELLO['id'] = 'Halo'
  HELLO['it'] = 'Ciao'
  HELLO['ja'] = 'こんにちは'
  HELLO['ka'] = 'გამარჯობა'
  HELLO['kx'] = 'nuqneH'
  HELLO['ko'] = '안녕하세요'
  HELLO['lv'] = 'labdien'
  HELLO['lt'] = 'labas'
  HELLO['ms'] = 'Hello'
  HELLO['mt'] = 'hello'
  HELLO['no'] = 'hallo'
  HELLO['fa'] = 'سلام'
  HELLO['pl'] = 'Cześć'
  HELLO['pt'] = 'Olá'
  HELLO['ro'] = 'bună'
  HELLO['ru'] = 'Привет'
  HELLO['sk'] = 'dobrý deň'
  HELLO['sl'] = 'zdravo'
  HELLO['es'] = '¡Hola!'
  HELLO['sv'] = 'Hej'
  HELLO['th'] = 'สวัสดี'
  HELLO['tr'] = 'Merhaba'
  HELLO['uk'] = 'Вітаю'
  HELLO['ur'] = 'اسلام و عليکم'
  HELLO['vi'] = 'Xin chào'
  HELLO['cy'] = 'Helo'
  HELLO['sr'] = 'Здраво'
  HELLO['hr'] = 'Ćao'
  HELLO['la'] = 'ave'

  class << self
    def by_language language
      HELLO[language.iso_code]
    end

    def by_iso iso_code
      HELLO[iso_code]
    end
  end
end
