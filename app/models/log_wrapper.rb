class LogWrapper
  include Responders::Log



  def initialize(controller, resource, request, options={})
    @controller = controller
    @resource = resource
    @log = options.delete(:log)
    @options = options
    @request = request
    logit if enabled?
  end

  def controller
    @controller
  end

  def resource
    @resource
  end

  def options
    @options
  end

  def request
    @request
  end

end
