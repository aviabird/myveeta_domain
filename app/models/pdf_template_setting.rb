# == Schema Information
#
# Table name: pdf_template_settings
#
#  id                     :uuid             not null, primary key
#  ext_id                 :integer
#  pdf_template_type      :string
#  color                  :string
#  font_face              :string
#  name                   :string
#  page_format            :string
#  format                 :integer
#  line_height            :string
#  template               :string
#  resume_id              :uuid
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  last_generated_at      :datetime
#  template_identifier    :string
#  custom_render_settings :text
#

class PdfTemplateSetting < ActiveRecord::Base
  extend Enumerize
  belongs_to :resume


  before_save :update_settings_from_base_template

  validates :pdf_template_type, :color, :template, :resume, :font_face, :name, :page_format, :line_height, presence: true

  validate :supported_color

  enumerize :pdf_template_type, in: %w(myveeta kickresume myveeta_base kickresume_base), predicates: true

  enumerize :template, in: %w(resume blurred dotts polygon red red_serif), predicates: true

  def trigger_pdf_generation
    if is_myveeta_template?
      generate_myveeta_pdf
    elsif is_kickresume_template?
      Resque.enqueue(KickresumePDFGenerationJob, resume.id)
    end
  end

  def trigger_pdf_generation_sync
    if is_myveeta_template?
      generate_myveeta_pdf
    elsif is_kickresume_template?
      KickresumePDFGenerationJob.generate_pdf resume
    end
  end

  def is_myveeta_template?
    pdf_template_type == :myveeta
  end

  def is_kickresume_template?
    pdf_template_type == :kickresume
  end

  def update_settings_from_base_template
    base = PdfTemplateSetting.where(template: template, pdf_template_type: "#{pdf_template_type}_base").first
    if base
      self.format = base.format
      self.template_identifier = base.template_identifier
      self.font_face = base.font_face
      self.custom_render_settings = base.custom_render_settings
      self.page_format = base.page_format
      self.line_height = base.line_height
    end
  end

  class << self
    def default resume
      settings = PdfTemplateSetting.new
      settings.template = "resume" #name of .html.erb file in pdfs folder
      settings.resume = resume
      settings.name = resume.name
      settings.pdf_template_type = :myveeta
      settings.color = "petrol"
      base = PdfTemplateSetting.where(template: settings.template, pdf_template_type: "#{settings.pdf_template_type}_base").first
      if base
        settings.format = base.format
        settings.font_face = base.font_face
        settings.page_format = base.page_format
        settings.line_height = base.line_height
      else
        settings.font_face = "sans serif"
        settings.page_format = "A4"
        settings.line_height = "normal"
      end
      settings
    end
  end


  def supported_color
    base = PdfTemplateSetting.where(template: template, pdf_template_type: "#{pdf_template_type}_base").first
    unless base && base.color.split(",").include?(self.color)
      errors.add(:color, "not supported for this template")
    end
  end

  def kickresume_color_lookup
    case "#{template}-#{color}"
    when "ios-blu"
      return "#03a9f4"
    when "ios-pin"
      return "#ff4081"
    when "ios-cya"
      return "#2bb9dd"
    else
      return "black"
    end
  end

  private

    def generate_myveeta_pdf
      # render template
      ac = ApplicationController.new()
      resume.pdf_template_setting.template
      user = resume.talent
      pdf = WickedPdf.new.pdf_from_string(
        ac.render_to_string(
          "pdfs/#{resume.pdf_template_setting.template_identifier}.html.erb",
          layout: false,
          :locals => {:resume => resume,
            :color => myveeta_color_lookup(resume.pdf_template_setting.color)}),
        footer: { :left => "#{user.first_name} #{user.last_name} | #{user.email} | #{user.phone_number}", :right => 'Page [page] of [topage]' }
      )

      # save temp file
      tempfile = Tempfile.new([SecureRandom.urlsafe_base64(32), '.pdf'])
      tempfile.binmode
      tempfile.write pdf
      tempfile.close
      # upload to webdav
      resume.reload
      resume.pdf = File.open(tempfile.path)
      resume.save
      update_column(:last_generated_at, DateTime.now)
      tempfile.unlink
    end

    def myveeta_color_lookup color_name
      case color_name
      when "petrol"
        return "#4C868F"
      when "flamingo"
        return "#F15F4C"
      when "mint"
        return "#6CCDB3"
      else
        return "#4C868F"
      end
    end
end
