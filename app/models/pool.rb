# == Schema Information
#
# Table name: pools
#
#  id         :uuid             not null, primary key
#  company_id :uuid
#  name       :string
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_pools_on_company_id  (company_id)
#  index_pools_on_created_at  (created_at)
#

class Pool < ActiveRecord::Base
  belongs_to :company
  has_and_belongs_to_many :talents

  scope :in_company, ->(company) { where(company: company) }

  def talents_count
    talents.count
  end
end
