# == Schema Information
#
# Table name: recruiters
#
#  id           :uuid             not null, primary key
#  company_id   :uuid
#  email        :string           not null
#  first_name   :string
#  last_name    :string           not null
#  phone_number :string
#  deleted_at   :datetime
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_recruiters_on_company_id  (company_id)
#  index_recruiters_on_created_at  (created_at)
#  index_recruiters_on_deleted_at  (deleted_at)
#  index_recruiters_on_email       (email)
#  index_recruiters_on_last_name   (last_name)
#

class Recruiter < ActiveRecord::Base
  ## modules
  include Concerns::UserAbility
  acts_as_paranoid
  acts_as_tagger

  ## associations
  belongs_to :company, inverse_of: :recruiters
  has_many :talent_infos, class_name: 'RecruiterTalentInfo'
  has_one :setting, class_name: 'RecruiterSetting'
  has_many :comments, class_name: 'RecruiterComment'
  has_one :auth, class_name: 'RecruiterAuth', inverse_of: :recruiter
  has_many :shortlists
  has_many :talents_on_shortlist, through: :shortlists, source: :talent

  ## validations
  validates :email, :last_name, :auth, presence: true
  validates :email, uniqueness: { case_sensitive: false , message: "talent.ui.auth.email.already_exists"}

  alias_method :recruiter_auth, :auth

  accepts_nested_attributes_for :auth, allow_destroy: true


  def ability
    @ability ||= Ability::RecruiterAbility.new(self)
  end

  def name
    first_name + ' ' + last_name
  end

  # Placeholder for recruiter's IP
  def ip
    '10.0.0.1'
  end

  def name
    "#{first_name} #{last_name}"
  end
end
