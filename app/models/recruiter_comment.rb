# == Schema Information
#
# Table name: recruiter_comments
#
#  id             :integer          not null, primary key
#  company_id     :uuid             not null
#  commented_id   :integer          not null
#  commented_type :string           not null
#  recruiter_id   :uuid
#  content        :text
#  deleted_at     :datetime
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_recruiter_comments_on_commented_id    (commented_id)
#  index_recruiter_comments_on_commented_type  (commented_type)
#  index_recruiter_comments_on_company_id      (company_id)
#  index_recruiter_comments_on_deleted_at      (deleted_at)
#

class RecruiterComment < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :company
  belongs_to :recruiter

  belongs_to :commented, polymorphic: true

  validates :commented, :company, presence: true
end
