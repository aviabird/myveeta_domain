# == Schema Information
#
# Table name: recruiter_settings
#
#  id                  :integer          not null, primary key
#  recruiter_id        :uuid
#  additional_settings :text
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
# Indexes
#
#  index_recruiter_settings_on_recruiter_id  (recruiter_id)
#

class RecruiterSetting < ActiveRecord::Base
  belongs_to :recruiter

  validates :recruiter, presence: true
end
