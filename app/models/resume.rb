# == Schema Information
#
# Table name: resumes
#
#  id                       :uuid             not null, primary key
#  talent_id                :uuid             not null
#  language_id              :integer          not null
#  name                     :string           not null
#  deleted_at               :datetime
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  work_experience_duration :float
#  origin_id                :uuid
#  version                  :integer          default(1)
#  origin_updated_at        :datetime
#  pdf                      :string
#  token                    :string
#
# Indexes
#
#  index_resumes_on_created_at   (created_at)
#  index_resumes_on_deleted_at   (deleted_at)
#  index_resumes_on_language_id  (language_id)
#  index_resumes_on_name         (name)
#  index_resumes_on_talent_id    (talent_id)
#

class Resume < ActiveRecord::Base
  acts_as_paranoid

  attr_accessor :validate_talent

  belongs_to :talent
  belongs_to :language
  belongs_to :origin, class_name: 'Resume'
  belongs_to :origin_with_deleted,-> { with_deleted }, :class_name => "Resume", :foreign_key => 'origin_id'

  include Concerns::HasLanguage
  has_many :jobapps
  has_many :companies, through: :sharings
  has_many :sharings, class_name: 'ResumeSharing'
  has_many :veeta_button_sharings, class_name: 'VeetaButtonSharing'
  has_many :sharing_requests, class_name: 'ResumeSharingRequest'
  has_many :pdfs, class_name: 'ResumePdf'
  has_many :versions, class_name: 'Resume', foreign_key: 'origin_id'
  has_many :origins_resumes, class_name: 'Resume', foreign_key: 'origin_id'

  validates :name, :language_id, presence: true, on: :update
  validates :talent, presence: true, if: :validate_talent

  has_many :work_experiences, class_name: 'Rs::WorkExperience'
  has_many :educations, class_name: 'Rs::Education'
  has_many :languages, class_name: 'Rs::Language'
  has_many :skills, -> {app_sorting_order}, class_name: 'Rs::Skill'
  has_many :documents, class_name: 'Rs::Document'
  has_many :memberships, class_name: 'Rs::Membership'
  has_many :projects, class_name: 'Rs::Project'
  has_many :awards, class_name: 'Rs::Award'
  has_many :publications, class_name: 'Rs::Publication'
  has_many :certifications, class_name: 'Rs::Certification'
  has_one :about, class_name: 'Rs::About'
  has_one :link, class_name: 'Rs::Link'
  has_one :pdf_template_setting, class_name: 'PdfTemplateSetting'

  accepts_nested_attributes_for :talent

  mount_uploader :pdf, CvUploader

  scope :my, ->(talent) { where(talent: talent, origin: nil).order('updated_at DESC') }
  scope :dummy, -> { where(id: 1) }
  scope :original_and_versions, -> (resume) { where("id = ? OR origin_id = ?", resume.id, resume.id) }
  scope :original_only, -> (resume) { find_by(id: resume.origin_id, version: 1) }
  scope :find_version, -> (resume, version) { find_by(origin_id: resume.id, version: version) }
  scope :find_parent, -> (resume) { find_by(id: resume.origin_id) }
  scope :find_all_versions, -> (resume) { where(origin_id: resume.id) }
  scope :all_versions_of_origins, -> (origins) { where("origin_id in (?)", origins.pluck(:id)) }

  amoeba do
    enable
    include_association :work_experiences
    include_association :educations
    include_association :languages
    include_association :skills
    include_association :documents
    include_association :memberships
    include_association :projects
    include_association :awards
    include_association :publications
    include_association :certifications
    include_association :about
    include_association :link
    include_association :pdf_template_setting
  end

  def version_count
    Resume.where(origin_id: id).count
  end

  def versions_with_jobapp
    origin.versions.each { |v| Jobapp.find_by(resume_id: v.id) } if version != 1
  end

  # Dummy link to resume preview
  def image_snapshot_url
  	nil
  end

  # Find companies which have access to this resume via some job application
  def companies
    companies = []
    latest_sharings.map do |ls|
      if ls.company
        companies << ls.company
      elsif ls.email_application
        companies << ls.email_application.company
      end
    end
    companies
    #@companies ||= list_companies
  end
  alias_method :shared_with, :companies

  # # Directly shared with
  # def directly_shared_with
  #   sharings.map(&:company)
  # end

  # Decode medium size image
  def base64_avatar
    if talent.avatar.medium.path
      Base64.encode64(open(talent.avatar.medium.path).read)
    end
  end

  def clean
    work_experiences.each{ |we| we.destroy }
    educations.each{ |e| e.destroy }
    certifications.each{ |c| c.destroy }
    languages.each{ |l| l.destroy }
    skills.each{ |s| s.destroy }
    unless about.nil?
      about.destroy
    end
    unless link.nil?
      link.destroy
    end
    save
  end

  # Fillin informations from PDF
  def fillin pdf
    if pdf.valid?
      clean
      ResumeService::PrefillPDF.call(self, pdf)
    end
  end

  # Dummy sequence
  def sequence
    talent.resumes.count + 1
  end

  def work_industries
    work_experiences.map(&:industry)
  end

  def work_experience_duration
    if work_experiences
      experiences = []
      work_experiences.each do |we|
        experiences << { from: we.from, to: we.validated_to, part: we.part_time? }
      end
      @work_experience_duration = WorkExperienceCalculator.working_duration(experiences, in: :days)
    end
  end

  def international_work_experience_durations
    if work_experiences
      experiences = []
      work_experiences.each do |we|
        experiences << { from: we.from, to: we.validated_to, part: we.part_time?, iso_code: we.country.iso_code }
      end
      @international_work_experience_durations = WorkExperienceCalculator.international_duration(experiences, in: :days)
    end
  end

  def latest_work_experience
    if work_experiences.find_by(to: nil)
      work_experiences.where(to: nil).order(from: :asc).first
    else
      work_experiences.order(to: :desc).first
    end unless work_experiences.empty?
  end

  # # Prefil new instance of resume with data from talent
  def prefill current_talent
    self.talent = current_talent
    self.language = Language.find(I18n.locale)
    self.name = I18n.t "talent.ui.resume.default_name", resumeCount: Resume.with_deleted.where(talent: current_talent, origin: nil).count + 1
    self.pdf_template_setting = PdfTemplateSetting.default self
  end

  def document_urls
    documents.map(&:url)
  end

  # Ruby 2.1 required keyword arguments
  def share parameters
    # || true for compatibility, default is real company
    company_type =  parameters.has_key?(:real_company) ? parameters[:real_company]  : "REAL"
    if(parameters[:company_ids])
      Share.call self, parameters[:company_ids], company_type
      # email_update_to_company self, company_ids
    end
  end

  def sharing_request
      Share.clone self
      latest
  end

  # After Talent shares updates with exiting companies, send email to company recruiters or owner
  # def email_update_to_company resume, company_ids
  #   company_ids.each do |id|
  #     company = Company.find(id)
  #     if company.recruiters.exists?
  #       company.recruiters.each do |r|
  #         JobApplicationAndUpdateMailer.send_cv_update(resume, r).deliver
  #       end
  #     else
  #       JobApplicationAndUpdateMailer.send_cv_update(resume, company.owner).deliver
  #     end
  #   end
  # end

  # Find latest duplicated version
  def latest
    #should all other resumes be revoked when switching resume?
    self.class.where(origin: self).order(:version).last
  end

  #set updated_at only if there were changes in the resume
  def updated_at=(updated_at)
    latest_version = origin.nil? ? latest : self
    if !CloningService::ResumeComparison.call(self,latest_version)
#      resume.update_column(:updated_at, DateTime.now)
      write_attribute(:updated_at, DateTime.now)
    end
  end

  def all_versions
    self.class.where(origin: self)
  end

  def latest_sharings
    # get all versions ids for all origin_ids, order by created
    all_versions_ids = talent.versioned_resumes_ids
    #get sharings for versions
    all_sharings_of_talent = ResumeSharing.where(access_revoked_at: nil).where("resume_id in (?)",all_versions_ids).order(updated_at: :desc)

    sharings = []
    processed_company = []
    processed_email_company = []
    all_sharings_of_talent.each do |sharing|
      #generate a key of company/email_company ids in order to determine if this company was processed already
      key = sharing.company ? "c-#{sharing.company_id}" : "ec-#{sharing.email_application.recruiter_email}"
      # skip if already processed
      next if processed_company.include?(key)
      # is the latest sharing with this company part of this resume
      if sharing.resume.origin_id == id
        sharings << sharing
      end
      #company processed
      processed_company << key
    end
    sharings

    # get active connections and check latest synced sharing
    VeetaButtonSharingConnection.my_active(talent).each do |conn|
      if conn.latest_synced_sharing_of_connection && conn.latest_synced_sharing_of_connection.resume && conn.latest_synced_sharing_of_connection.resume.origin_id == id
        sharings << conn.latest_synced_sharing_of_connection
      end
    end
    sharings = sharings.sort! { |a,b| a.updated_at <=> b.updated_at }

    sharings
  end

  def safe_profile_picture
    #commented out, this would always use the current profile picture instead of the versioned one
    #origin ? origin.profile_picture : profile_picture
    profile_picture_small
  end

  def profile_picture_small
    if talent.avatar and talent.avatar_url and talent.avatar_url[:small] && file_exists?(talent.avatar_url[:small])
      talent.avatar.small.file.read
    else
      talent.generic_picture
    end
  end

  def profile_picture
    if talent.avatar and talent.avatar_url and talent.avatar_url[:medium] && file_exists?(talent.avatar_url[:medium])
      talent.avatar.medium.file.read
    else
      talent.generic_picture
    end
  end

  # if origin was deleted a version can be assumed to be deleted as well
  def deleted?
    origin.nil?
  end

  def origin_regardless_deletion
    if Resume.with_deleted.exists?(origin_id)
      Resume.with_deleted.find(origin_id)
    else
      nil
    end
  end

  def new_pdf_required?
    if origin_id.nil?
      #origin:
      pdf_template_setting.nil? || pdf_template_setting.last_generated_at.nil? || updated_at > pdf_template_setting.last_generated_at || pdf_template_setting.updated_at > pdf_template_setting.last_generated_at || talent.updated_at > pdf_template_setting.last_generated_at
    else
      #clone:
      pdf_template_setting.nil?
    end
  end

  def file_exists? url
    begin
      if url.starts_with? 'http'
        uri = URI(url)
        request = Net::HTTP.new(uri.host, uri.port)
        request.use_ssl = url.starts_with?('https') ? true : false
        request.verify_mode = OpenSSL::SSL::VERIFY_NONE
        response= request.request_head uri.path
        return response.code.to_i == 200
      elsif url.starts_with? '/'
        File.exist?(url) || File.symlink?(url)
      else
        false
      end
    rescue
      false
    end
  end

  def safe_pdf_template_setting
    if self.pdf_template_setting.nil?
      self.pdf_template_setting = PdfTemplateSetting.default self
      unless self.new_record?
        self.pdf_template_setting.save(validate: false)
        self.reload!
      end
    end
    self.pdf_template_setting
  end

  private
    # Browse companies in jobapps and in resume sharings
    def list_companies
      companies = []
      jobapps.each{ |jobapp| companies << jobapp.job.company if jobapp.job && jobapp.job.company && !companies.include?(jobapp.job.company) }
      sharings.each{ |sharing| companies << sharing.company if sharing.company && !companies.include?(sharing.company) }
      companies
    end



  class << self
    # Skip validations for talent when preparing new resume
    def prepare current_talent
      resume = current_talent.resumes.find{ |r| r if r.created_at == r.updated_at}
      unless resume
        resume = new
        resume.prefill(current_talent)
        resume.save(validate: false)
        #create clone of new resume in order to be able to track changes
        CloningService::ResumeClone.call(resume)
      end
      resume
    end

    def my_or_dummy talent
      talent.resumes.empty? ? dummy : my(talent)
    end



  end
end
