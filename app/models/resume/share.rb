class Resume::Share
  attr_reader :resume, :company_ids, :company_type

  # company_type can be REAL, APP, EMAIL
  def initialize resume, company_ids, company_type = "REAL"
    raise ArgumentError, 'company_ids needs to be array' unless company_ids.is_a?(Array)
    @resume = resume
    @company_ids = company_ids
    @company_type = company_type
  end

  # Perform sharing by cloning resume if necessary and sharing
  # latest resume with given companies
  def call
    clone
    if company_type == "REAL"
      share
    elsif company_type == "APP"
      share_via_veeta_button
    else
      share_with_email_application_company
    end
    self
  end

  # Clone resume if was updated
  def clone
    $lock = RemoteLock.new(RemoteLock::Adapters::Redis.new(Redis.new))
    $lock.synchronize("CloningService::ResumeClone-#{resume.id}", retries: 12, initial_wait: 1) do
      resume.reload
      CloningService::ResumeClone.call(resume) if different?
    end
  end

  private
    # Compare resume with the latest version to see, if there were updates
    # When sharing a resume, also consider applicant information for changes
    def compare
      CloningService::ResumeComparison.new(resume, resume.latest).call_for_cloning
    end

    # Check if resumes are different using compare method
    def different?
      resume and resume.latest ? !compare : true
    end

    # Share the latest version with requested companies
    def share
      latest = resume.latest
      ass = AccessSecurityService.new
      company_ids.each do |id|
        if Company.exists?(id)
          r = ResumeSharing.find_or_initialize_by(resume: latest, company_id: id, access_revoked_at: nil)
          r.updated_at = DateTime.now
          unless CompanyTalentInfo.exists?(company_id: id,talent_id: resume.talent_id)
            CompanyTalentInfo.create(company_id: id,talent_id: resume.talent_id)
          end
          r.save!
          ass.establish_sharing r
        end
      end
    end

    # Share the latest version with requested companies
    def share_with_email_application_company
      latest = resume.latest
      ass = AccessSecurityService.new
      #iterate over email_application_id
      #TODO change to email application id for better understanding
      company_ids.each do |id|
        if EmailApplication.exists?(id)
          r = ResumeSharing.find_or_initialize_by(resume: latest, email_application_id: id)
          r.updated_at = DateTime.now
          r.save!
          ass.establish_sharing r
        end
      end
    end

    # share with client application connected via veeta button
    def share_via_veeta_button
      latest = resume.latest
      ass = AccessSecurityService.new
      company_ids.each do |id|
        if ClientApplication.exists?(id)
          vbsc = VeetaButtonSharingConnection.find_by(talent_id: resume.talent.id,client_application_id: id, access_revoked_at: nil)
          latest_sharing = vbsc.latest_sharing_of_connection
          if latest_sharing.resume.blank? || latest.id != latest_sharing.resume.id
            r = VeetaButtonSharing.new(resume: latest,veeta_button_sharing_connection_id: vbsc.id)
            r.save!
            ass.establish_sharing r
          end
        end
      end
    end

    def self.call resume, company_ids, company_type = "REAL"
      new(resume, company_ids, company_type).call
    end

    def self.clone resume
      new(resume, []).clone
    end
end
