# == Schema Information
#
# Table name: resume_pdfs
#
#  id         :integer          not null, primary key
#  resume_id  :uuid
#  file       :string
#  file_tmp   :string
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_resume_pdfs_on_resume_id  (resume_id)
#

class ResumePdf < ActiveRecord::Base
  belongs_to :resume

  validates :resume, :file, presence: true

  mount_uploader :file, ResumePdfUploader
end
