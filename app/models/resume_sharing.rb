# == Schema Information
#
# Table name: resume_sharings
#
#  id                   :integer          not null, primary key
#  resume_id            :uuid             not null
#  company_id           :uuid
#  recruiter_id         :uuid
#  access_revoked_at    :datetime
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  revoke_reason        :string
#  email_application_id :uuid
#
# Indexes
#
#  index_resume_sharings_on_access_revoked_at  (access_revoked_at)
#  index_resume_sharings_on_company_id         (company_id)
#  index_resume_sharings_on_recruiter_id       (recruiter_id)
#  index_resume_sharings_on_resume_id          (resume_id)
#

class ResumeSharing < ActiveRecord::Base
  belongs_to :resume
  has_one :origin, through: :resume, source: :origin
  belongs_to :company
  belongs_to :recruiter
  belongs_to :email_application

  validates :resume, presence: true

  scope :nonrevoked, -> { where(access_revoked_at: nil) }
  #query on  updated_at field
  scope :last_intervall, -> (last_synced_at) { where(updated_at: last_synced_at..DateTime.now.utc, access_revoked_at: nil).order('updated_at DESC') }


  # Revoke access by flagging as revoked
  def revoke_jobapps talent, revoke_reason = nil
    self.update_columns(access_revoked_at: DateTime.now, revoke_reason: revoke_reason)
    if company
      #set revoked_at for alls jobapps to this company
      vids = talent.versioned_resumes_ids
      #ensure that only applications of the current user are affected by going via resume id
      jobapps_to_revoke = Jobapp.joins(:real_job).where("company_id = (?)",company.id).where(access_revoked_at: nil).where("resume_id in (?)", vids)
      cv_sharing_response_to_revoke = CvSharingResponse.joins(:real_cv_sharing_request).where("company_id = (?)",company.id).where(access_revoked_at: nil).where("resume_id in (?)", vids)
      (jobapps_to_revoke + cv_sharing_response_to_revoke).each do |ja|
        ja.update_columns(access_revoked_at: DateTime.now, revoke_reason: revoke_reason)
      end
    else
      #jobapp via email_application
      email_application.jobapp.update_columns(access_revoked_at: DateTime.now)
    end
  end

  def revoked?
    access_revoked_at?
  end

  # comparison of timestamps should be enough, as updated_at for resume is only set when changed were made
  def compare
    CloningService::ResumeQuickComparison.call(origin, resume)

    #CloningService::ResumeComparison.call(origin, resume)
  end

  # def other_resume_jobapps
  #   resume.jobapps.count > 0
  # end

  def company_talent_info
    orig = Resume.with_deleted.find(resume.origin_id)
    orig.talent.company_infos.where(company_id: company.id).first
  end

  alias_method :latest?, :compare

  def results_from
    results_from = false
    if(company)
      results_from = Jobapp.joins(:real_job).where(jobs: {company_id: company.id},resume_id: resume.id).count > 0 ? :jobapp : false
      results_from = (CvSharingResponse.joins(:real_cv_sharing_request).where(cv_sharing_requests: {company_id: company.id},resume_id: resume.id, update_only: false).count > 0 ? :cv_sharing_response : false) unless results_from
    else
      results_from = Jobapp.joins(:email_application).where(email_applications: {recruiter_email: email_application.recruiter_email},resume_id: resume.id).count > 0 ? :email_application : false
    end
    if !results_from
      results_from = :update
    end
    results_from
  end

  class << self
    # Find all sharings for user
    def find_with_company_by_user user
      versioned_resumes_ids = user.versioned_resumes_ids
      joins(:company)
        .where(access_revoked_at: nil).where("resume_id in (?)",versioned_resumes_ids)
        .order('updated_at DESC')
    end

    def find_with_email_app_by_user user
      versioned_resumes_ids = user.versioned_resumes_ids
      includes(:email_application)
        .where(access_revoked_at: nil).where("resume_id in (?)",versioned_resumes_ids)
        .order('resume_sharings.updated_at DESC')
    end

    def find_by_user user
      [find_with_company_by_user(user), find_with_email_app_by_user(user)].flatten.sort! { |a,b| b[:updated_at] <=> a[:updated_at] }
    end

    # Get list of most updated sharings per company and user
    # Either query companies or email applications, which are unique by their email address
    def unique_by_user user
      a = []
      find_by_user(user).each{ |s| a << s unless  a.find{ |a| (a.company_id == s.company_id && s.email_application.nil?) || (a.email_application_id == s.email_application_id && s.company.nil?) || (!a.email_application.nil? && !s.email_application.nil? && a.email_application.recruiter_email == s.email_application.recruiter_email && s.company.nil?) } }
      a
    end

    #revoke existing company
    def revoke talent, company, options = {}
      sharings_to_revoke = ResumeSharing.where(company_id: company.id, access_revoked_at: nil, resume_id: Resume.all_versions_of_origins(talent.resumes.with_deleted).pluck(:id).to_a)
      latest_date = DateTime.new(0)
      latest_id = 0
      sharings_to_revoke.each do |sharing|
        sharing.revoke_jobapps talent, options[:revoke_reason]
        latest_id = sharing.id if latest_date < sharing.updated_at
      end
      AccessSecurityService.new.revoke_access sharings_to_revoke
      Resque.enqueue_in(ENV['INSTANT_MAIL_DELAY'].to_i, MailRevokedAccessJob, I18n.locale, 'company', company.id, talent.id, options[:revoke_reason], latest_id)
    end

    #revoke email application company
    def revoke_email_application talent, email_application, options = {}
      sharings = ResumeSharing.where(email_application_id: email_application.id, access_revoked_at: nil, resume_id: Resume.all_versions_of_origins(talent.resumes.with_deleted).pluck(:id).to_a)
      latest_date = DateTime.new(0)
      latest_id = 0
      sharings.each do |sharing|
        sharing.revoke_jobapps talent, options[:revoke_reason]
        latest_id = sharing.id if latest_date < sharing.updated_at
      end
      AccessSecurityService.new.revoke_access sharings
      Resque.enqueue_in(ENV['INSTANT_MAIL_DELAY'].to_i, MailRevokedAccessJob, I18n.locale, 'emailcompany', email_application.id, talent.id, options[:revoke_reason], latest_id)
    end

    # Get list of uniques sharings for comapny and cv in last day
    def ready_to_share last_synced_at
      sharings = []
      company_blacklist = []
      email_company_blacklist = []
      #iterate sharings
      last_intervall(last_synced_at).each do |recent_sharing|

        #skip a sharing if a sharing via application was created more recently
        next if !recent_sharing.company.nil? && company_blacklist.include?(recent_sharing.company.id)
        next if !recent_sharing.email_application.nil? && email_company_blacklist.include?(recent_sharing.email_application.recruiter_email)

        #sharing with company
        if sharings.select{|s| s.company == recent_sharing.company && s.email_application.nil? }.empty? && recent_sharing.email_application.nil?

          #if  most recent sharing results from jobapp/sharing response, skip further sharing notifications
          if !recent_sharing.company.nil? && recent_sharing.results_from != :update
            company_blacklist << recent_sharing.company.id
          else
            sharings << recent_sharing
          end
        end

        #sharing with email application company
        if !recent_sharing.email_application.nil? && recent_sharing.company.nil? && sharings.select{|s| !s.email_application.nil? && s.email_application.recruiter_email == recent_sharing.email_application.recruiter_email }.empty?

          #if  most recent sharing results from jobapp/sharing response, skip further sharing notifications
          if !recent_sharing.email_application.recruiter_email.nil? && recent_sharing.results_from != :update
            email_company_blacklist << recent_sharing.email_application.recruiter_email
          else
            sharings << recent_sharing
          end
        end
      end
      sharings
    end
  end
end
