# == Schema Information
#
# Table name: resume_sharing_requests
#
#  id           :uuid             not null, primary key
#  email        :string
#  contact_name :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  resume_id    :uuid             not null
#
# Indexes
#
#  index_resume_sharing_requests_on_created_at  (created_at)
#  index_resume_sharing_requests_on_email       (email)
#

class ResumeSharingRequest < ActiveRecord::Base
  belongs_to :resume
  has_one :origin, through: :resume, source: :origin


  validates :contact_name, :email, presence: true
  validates :resume, presence: true

  scope :my, ->(talent) { joins({resume: :origin}).where(origins_resumes: {talent_id: talent.id}) }
  scope :last_intervall, -> { where(updated_at: Rails.configuration.update_intervall.ago..DateTime.now.utc).order('updated_at DESC') }

  def my_type
    'SharingRequest'
  end

  def origin_resume
    resume.origin if resume
  end

  #expose which type of activity/jobapp this object is
  alias_method :job_type, :my_type

  class << self

    def my_sharings talent_id
      origin_ids = Resume.with_deleted.select(:id).where(talent_id: talent_id).to_a
      all_versions_ids = Resume.select(:id).where("origin_id in (?)",origin_ids).to_a
      ResumeSharingRequest.where(resume_id: all_versions_ids)
    end

  end

end
