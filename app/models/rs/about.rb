# == Schema Information
#
# Table name: rs_abouts
#
#  id         :uuid             not null, primary key
#  resume_id  :uuid
#  text       :text
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_rs_abouts_on_created_at  (created_at)
#  index_rs_abouts_on_resume_id   (resume_id)
#

class Rs::About < ActiveRecord::Base
  include Concerns::RsDefault
  
  belongs_to :resume
end
