# == Schema Information
#
# Table name: rs_awards
#
#  id          :uuid             not null, primary key
#  resume_id   :uuid
#  name        :string
#  occupation  :string
#  awarded_by  :string
#  year        :datetime
#  description :text
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_rs_awards_on_created_at  (created_at)
#  index_rs_awards_on_resume_id   (resume_id)
#

class Rs::Award < ActiveRecord::Base
  include Concerns::RsDefault
  
  belongs_to :resume

  validates :name, :awarded_by, presence: true
end
