# == Schema Information
#
# Table name: rs_certifications
#
#  id                 :uuid             not null, primary key
#  resume_id          :uuid
#  subject            :string
#  organization       :string
#  description        :text
#  created_at         :datetime
#  updated_at         :datetime
#  certification_type :string
#  year               :integer
#
# Indexes
#
#  index_rs_certifications_on_created_at  (created_at)
#  index_rs_certifications_on_resume_id   (resume_id)
#

class Rs::Certification < ActiveRecord::Base
  include Concerns::RsDefault
  extend Enumerize

  default_scope { order(year: :DESC, subject: :ASC) }

  belongs_to :resume

  validates :certification_type, :subject, :year, presence: true

  validates :year, numericality: { only_integer: true }

  enumerize :certification_type, in: %w(certification continued_education), predicates: true
end
