# == Schema Information
#
# Table name: rs_documents
#
#  id            :uuid             not null, primary key
#  resume_id     :uuid
#  file          :string
#  document_type :string
#  created_at    :datetime
#  updated_at    :datetime
#  name          :string
#  file_type     :string
#  token         :string
#
# Indexes
#
#  index_rs_documents_on_created_at  (created_at)
#  index_rs_documents_on_resume_id   (resume_id)
#  index_rs_documents_on_token       (token)
#

class Rs::Document < ActiveRecord::Base
  include Concerns::RsDefault
  include Concerns::AccessedByExternal
  extend Enumerize

  enumerize :document_type, in: [:reference,
                                 :recommendation,
                                 :credentials,
                                 :work_permit,
                                 :certificate,
                                 :others]

  mount_uploader :file, DocumentUploader

  belongs_to :resume

  validates :resume, :file, presence: true

  delegate :url, to: :file

  delegate :store_dir, to: :file

  delegate :refresh_token, to: :file

  def external_url external_entity
    secure_access_token = external_entity.access_path_token self
    "#{ENV['HOST']}#{parpare_external_path(file.path,secure_access_token)}"
  end
end
