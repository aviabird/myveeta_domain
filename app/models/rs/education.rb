# == Schema Information
#
# Table name: rs_educations
#
#  id                :uuid             not null, primary key
#  resume_id         :uuid             not null
#  type_of_education :string
#  school_name       :string
#  degree            :string
#  subject           :string
#  country_id        :integer
#  from              :date
#  to                :date
#  description       :text
#  created_at        :datetime
#  updated_at        :datetime
#  completed         :boolean
#
# Indexes
#
#  index_rs_educations_on_country_id         (country_id)
#  index_rs_educations_on_created_at         (created_at)
#  index_rs_educations_on_from               (from)
#  index_rs_educations_on_resume_id          (resume_id)
#  index_rs_educations_on_school_name        (school_name)
#  index_rs_educations_on_subject            (subject)
#  index_rs_educations_on_to                 (to)
#  index_rs_educations_on_type_of_education  (type_of_education)
#

class Rs::Education < ActiveRecord::Base
  include Concerns::RsDefault

  scope :from_start, -> { order(:from).order(:to).reverse_order }


  belongs_to :resume
  belongs_to :country
  include Concerns::HasCountry

  validates :from, date: { before_or_equal_to: :validated_to }, :allow_blank => true, on: :update
  validates :to, date: { after_or_equal_to: :from }, allow_nil: true, on: :update

  validates :school_name, :type_of_education, presence: true, on: :update

  # Method to pass validator if to is nill
  def validated_to
  	@to || DateTime.now
  end

  # Determinate if the school is current
  def current
  	to == nil ? true : false
  end
end
