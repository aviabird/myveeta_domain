# == Schema Information
#
# Table name: rs_languages
#
#  id          :uuid             not null, primary key
#  resume_id   :uuid
#  language_id :integer
#  level       :integer
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_rs_languages_on_created_at   (created_at)
#  index_rs_languages_on_language_id  (language_id)
#  index_rs_languages_on_level        (level)
#  index_rs_languages_on_resume_id    (resume_id)
#

class Rs::Language < ActiveRecord::Base
  include Concerns::RsDefault
  attr_accessor :languge_name

  belongs_to :resume
  belongs_to :language
  include Concerns::HasLanguage

  #scope :pdf_sorting_order, -> {  order(level: :DESC, language_name: :ASC) }

  delegate :name,
           :iso_code,
           to: :language

  validates :language_id, :level, presence: true


  def sort_helper
    "#{(level-5).abs}-#{name}"
  end


  def level_key
    case level
    when 1
      'beginner'
    when 2
      'intermediate'
    when 3
      'fluent'
    when 4
      'business_fluent'
    when 5
      'native'
    else
      'undefined'
    end
  end

end
