# == Schema Information
#
# Table name: rs_publications
#
#  id               :uuid             not null, primary key
#  resume_id        :uuid
#  title            :string
#  publication_type :string
#  year             :datetime
#  description      :text
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_rs_publications_on_created_at  (created_at)
#  index_rs_publications_on_resume_id   (resume_id)
#

class Rs::Publication < ActiveRecord::Base
  include Concerns::RsDefault
  
  belongs_to :resume

  validates :title, :publication_type, :year, presence: true
end
