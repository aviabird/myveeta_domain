# == Schema Information
#
# Table name: rs_skills
#
#  id         :uuid             not null, primary key
#  resume_id  :uuid
#  name       :string
#  level      :integer
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_rs_skills_on_created_at  (created_at)
#  index_rs_skills_on_level       (level)
#  index_rs_skills_on_name        (name)
#  index_rs_skills_on_resume_id   (resume_id)
#

class Rs::Skill < ActiveRecord::Base
  include Concerns::RsDefault

  belongs_to :resume

  validates :name, presence: true

  scope :app_sorting_order, -> { order('coalesce(level,-1) desc, created_at desc') }

  scope :pdf_sorting_order, -> {  reorder('coalesce(level,-1) DESC, name ASC') }

  def safe_level
    level.blank? ? 0 : level
  end

  def level_key
    case level
    when 1
      'beginner'
    when 2
      'intermediate'
    when 3
      'advanced'
    when 4
      'expert'
    else
      nil
    end
  end
end
