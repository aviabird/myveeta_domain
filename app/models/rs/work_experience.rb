# == Schema Information
#
# Table name: rs_work_experiences
#
#  id                  :uuid             not null, primary key
#  resume_id           :uuid             not null
#  position            :string
#  company             :string
#  industry            :string
#  job_level           :string
#  terms_of_employment :string
#  from                :datetime
#  to                  :datetime
#  country_id          :integer
#  description         :text
#  created_at          :datetime
#  updated_at          :datetime
#
# Indexes
#
#  index_rs_work_experiences_on_company     (company)
#  index_rs_work_experiences_on_country_id  (country_id)
#  index_rs_work_experiences_on_created_at  (created_at)
#  index_rs_work_experiences_on_from        (from)
#  index_rs_work_experiences_on_industry    (industry)
#  index_rs_work_experiences_on_position    (position)
#  index_rs_work_experiences_on_resume_id   (resume_id)
#  index_rs_work_experiences_on_to          (to)
#

class Rs::WorkExperience < ActiveRecord::Base
  include Concerns::RsDefault
  extend Enumerize

  scope :from_start, -> { order(:from).order(:to).reverse_order }

  enumerize :terms_of_employment, in: %w(full_time part_time self_employed)
  enumerize :industry, in: %w(AgriFish Arts AuditTaxLaw Automotive Construction Consulting Craft EduScience Electronics EnergyWater Engineering Entertainment Finance FoodTourism HR ITSvcSoftware Luxury Manufacturing MarketingDesign Media Medical MiningFuel Public RealEstate Security Social Tech Telco Trade Transport Other)
  enumerize :job_level, in: %w(intern starter experienced executive)

  belongs_to :resume
  belongs_to :country
  include Concerns::HasCountry

  validates :from, date: {before_or_equal_to: :validated_to},:allow_blank => true, on: :update
  validates :to, date: {after_or_equal_to: :from}, allow_nil: true, on: :update
  validates :company, :position, presence: true, on: :update

  # Method to pass validator if to is nill
  def validated_to
    @to || Time.now
  end

  # Deteriminate if the position is current
  def current
    to == nil ? true : false
  end

  def part_time?
    terms_of_employment == 'part_time'
  end
end
