class Settings < Settingslogic
  source "#{Rails.root}/config/application.yml"
  namespace Rails.env

  class << self
    def api_url
      [base_url, api_url_suffix].join('/')
    end

    def api_talent_url
      [api_url, api_talent_url_suffix].join('/')
    end

    def api_recruiter_url
      [api_url, api_recruiter_url_suffix].join('/')
    end

    def api_admin_url
      [api_url, api_admin_url_suffix].join('/')
    end

    def app_url
      [base_url, app_url_suffix].join('/')
    end

    def app_talent_url
      [app_url, app_talent_url_suffix].join('/')
    end

    def app_recruiter_url
      [app_url, app_recruiter_url_suffix].join('/')
    end

    def app_admin_url
      [app_url, app_admin_url_suffix].join('/')
    end

    def faye_url
      [base_url, faye_url_suffix].join('/')
    end
  end
end