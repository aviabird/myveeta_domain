# == Schema Information
#
# Table name: shortlists
#
#  id           :uuid             not null, primary key
#  talent_id    :uuid
#  recruiter_id :uuid
#  created_at   :datetime
#  updated_at   :datetime
#
# Indexes
#
#  index_shortlists_on_created_at    (created_at)
#  index_shortlists_on_recruiter_id  (recruiter_id)
#  index_shortlists_on_talent_id     (talent_id)
#

class Shortlist < ActiveRecord::Base
  belongs_to :talent
  belongs_to :recruiter
end
