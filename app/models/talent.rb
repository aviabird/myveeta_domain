# == Schema Information
#
# Table name: talents
#
#  id                      :uuid             not null, primary key
#  first_name              :string
#  last_name               :string
#  birthdate               :datetime
#  avatar                  :string
#  salutation              :string
#  avatar_meta             :text
#  nationality_country_id  :integer
#  title                   :string
#  email                   :string           not null
#  phone_number            :string
#  address_id              :uuid
#  veeta_terms_accepted_at :datetime
#  veeta_terms_accepted_ip :string
#  locale                  :string
#  deleted_at              :datetime
#  delete_reason           :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  ext_id                  :string
#  new                     :boolean          default(TRUE)
#  token                   :string
#  military_service        :string
#
# Indexes
#
#  index_talents_on_avatar                  (avatar)
#  index_talents_on_birthdate               (birthdate)
#  index_talents_on_created_at              (created_at)
#  index_talents_on_deleted_at              (deleted_at)
#  index_talents_on_email                   (email)
#  index_talents_on_first_name              (first_name)
#  index_talents_on_last_name               (last_name)
#  index_talents_on_nationality_country_id  (nationality_country_id)
#  index_talents_on_salutation              (salutation)
#  index_talents_on_token                   (token)
#

class Talent < ActiveRecord::Base
  include Concerns::UserAbility
  include Concerns::AccessedByExternal
  include Concerns::HasExtId
  include TalentsIndex

  acts_as_paranoid
  acts_as_taggable_on :labels

  mount_uploader :avatar, AvatarUploader
  serialize :avatar_meta

  extend Enumerize

  #paginates_per 10

  enumerize :salutation, in: [:mr, :mrs]

  attr_accessor :temp_avatar_original_filename

  has_one :auth, class_name: 'TalentAuth', dependent: :destroy
  has_one :setting, class_name: 'TalentSetting', dependent: :destroy

  belongs_to :nationality_country, class_name: 'Country'
  belongs_to :address, class_name: 'Geo::Address'

  has_many :resumes
  has_many :companies, through: :resumes
  has_many :jobapps, through: :resumes
  has_many :jobs, through: :jobapps, source_type: :Job

  has_many :recruiter_comments, as: :commented

  has_many :recruiter_infos, class_name: 'RecruiterTalentInfo'
  has_many :company_infos, class_name: 'CompanyTalentInfo'

  has_and_belongs_to_many :pools

  accepts_nested_attributes_for :address

  validates :setting, presence: true

  # validates :first_name, :last_name, format: { with: /\A([ \u00c0-\u01ffa-zA-Z'\-])+\z/i }, allow_blank: true
  validates :first_name, :last_name, :phone_number, presence: true, on: :update
  validate :unique_email, on: :update
  # validates :nationality_country_id, presence: true
#  validates_format_of :email,:with => /\A([-a-z0-9öäüÖÄÜ\.]+)@((?:[-a-z0-9öäüÖÄÜ\.]+\.)+)([a-zA-Z]{2,4})\z/, on: :create

  delegate :guest?, to: :auth
  delegate :confirmed?, to: :auth
  delegate :store_dir, to: :avatar
  delegate :refresh_token, to: :avatar
  delegate :refresh_full_token, to: :avatar

  scope :in_company, ->(company) { all }



  after_save :update_resumes
  def update_resumes
    resumes.each do |r|
      # set updated_at for resume only if there were changes when having
      # profile data which is not yet reflected in a versioned cv (versioned talent)
      # the update date is set again on every save action of the talent
      latest_version = r.latest
      #the problem here is the following:
      if latest_version && !Talent.compare_talents(r.talent,latest_version.talent)# !CloningService::ResumeComparison.call(r,latest_version)
        # take updated_at dates from talent and address into account
        latest_update_date = []
        latest_update_date << updated_at
        # Change the following line for VEETA-1841 due to an image upload error.
        # I believe there was an error here and address.nil? should have been address.updated_at.nil?
        # latest_update_date << address.updated_at unless address.nil?
        latest_update_date << address.updated_at unless address.nil? || address.updated_at.nil?
        r.update_column(:updated_at, latest_update_date.max)
      end
    end
  end

  before_save :update_auth_email, on: :update, if: Proc.new { |t| t.email_changed? }
  def update_auth_email
    if auth
      auth.update(email: email, uid: email, confirmed_at: nil)
      auth.send_confirmation_instructions if !auth.guest?
    end
  end


  # before_save :propagate_email_changes, if: Proc.new { |t| t.email_changed? && t.auth }, on: :update
  # def propagate_email_changes
  #   puts 'propagate_email_changes'
  #   if auth
  #     auth.update(email: email)
  #   end
  # end

  amoeba do
    enable
    include_association :setting
    include_association :address
    customize(lambda { |original_talent,new_talent|
      new_talent.address = original_talent.address.amoeba_dup
      if original_talent.avatar
        begin
          new_talent.avatar = original_talent.avatar
        rescue
          # could not write avatar to webdav... fallback - remove avatar from cloned talent
          new_talent.avatar = nil
          new_talent.avatar_meta = nil
          new_talent.token = nil
          LogService.call 'Talent.amoeba', {talent: original_talent, details: "Failed to update avatar",priority: :high}
        ensure
          new_talent.save(validate:false)
          new_talent.update_column(:avatar, nil) if new_talent.token.blank?
        end
      end
    })
  end

  def uid
    #Hashids.new(ENV['UID_SALT']).encode(id)
  end

  def address
    super || build_address
  end

  def ability
    @ability ||= Ability::TalentAbility.new(self)
  end

  def accept_terms(ip)
    self.veeta_terms_accepted_at = DateTime.now
    self.veeta_terms_accepted_ip = ip
  end

  def sync_all=(status)
    # @todo sync profile to all resume
  end

  # Get full name
  def name
    if first_name && last_name
      first_name + ' ' + last_name
    elsif first_name
      first_name
    else
      last_name
    end
  end

  def generic_picture
    if salutation == 'mrs'
      File.read(Rails.root.join('app/assets/images/female-icon.png'))
    else
      File.read(Rails.root.join('app/assets/images/male-icon.png'))
    end
  end

  def profile_picture_url
    if avatar?
      avatar.small.url
    else
      if salutation == 'mrs'
        Rails.root.join('app/assets/images/female-icon.png')
      else
        Rails.root.join('app/assets/images/male-icon.png')
      end
    end
  end

  # Dummy avatar url
  # def avatar_url
  #   "http://media-cache-ec0.pinimg.com/236x/7f/75/6b/7f756bcb3481db56650768cc5fc0cf50.jpg"
  # end
  #TODO: check if unused?
  def temp_avatar_filename=(filename)
    temp_avatar = TempUpload::Avatar.find_by_file(filename)
    return if temp_avatar.nil?
    self.temp_avatar_original_filename = temp_avatar.file_meta[:original_filename]
    self.avatar = temp_avatar.file
  end

  # Get talent info just for one recruiter
  def recruiter_info recruiter
    recruiter_infos.find_by(recruiter: recruiter)
  end

  # Get talent info just for one recruiter
  def company_info company
    company_infos.find_by(company: company)
  end

  def company_rating company
    @company_info = company_info(company)
    @company_info.rating == nil ? 0 : @company_info.rating
  end

  def job_expectation
    @job_expectation ||= JobExpectation.new(self)
  end

  def age
    if birthdate
      now = Time.now.utc.to_date
      now.year - birthdate.year - ((now.month > birthdate.month || (now.month == birthdate.month && now.day >= birthdate.day)) ? 0 : 1)
    end
    #((Time.current - birthdate) / 3600 / 24 / 365).round if birthdate
  end

  def latest_resume
    resumes(order: :updated_at).first
  end

  def work_industries
    latest_resume.work_industries if latest_resume
  end

  def work_experience_duration
    latest_resume.work_experience_duration if latest_resume
  end

  def latest_work_experience
    latest_resume.latest_work_experience
  end

  def in_pools company
    pools.where(company: company)
  end

  def note company
    company_infos.find_by(company: company).try(:note)
  end

  def applied_for_jobs_in company
    jobs.where(company: company)
  end

  def applied_for_jobs_positions_in company
    applied_for_jobs_in(company).map(&:name)
  end

  def first_applied_for_jobs_in company
    applied_for_jobs_in(company).order('created_at ASC').first
  end

  def followed_by? recruiter
    recruiter_infos.find_by(recruiter: recruiter).try(:followed?)
  end

  # "dl/t/jw4lmhtm/nkt2utt0lojaty50tkfe5eskoy8im5ps/fm8hgx8t/thumb_adb19c86-4268-4731-8480-0f168976463d.png"
  def external_avatar_url external_entity
    secure_access_token = external_entity.access_path_token self
    {
      thumb: "#{ENV['HOST']}#{parpare_external_path(avatar.thumb.path,secure_access_token)}",
      small: "#{ENV['HOST']}#{parpare_external_path(avatar.small.path,secure_access_token)}",
      medium: "#{ENV['HOST']}#{parpare_external_path(avatar.medium.path,secure_access_token)}",
      large: "#{ENV['HOST']}#{parpare_external_path(avatar.large.path,secure_access_token)}",
      orginal: "#{ENV['HOST']}#{parpare_external_path(avatar.path,secure_access_token)}"
    } if avatar?
  end

  def avatar_url
    {
      thumb: avatar.thumb.url,
      small: avatar.small.url,
      medium: avatar.medium.url,
      large: avatar.large.url,
      orginal: avatar.url
    } if avatar?
  end

  def update_avatar image
    begin
      if validate_image image
        update_attribute :avatar, image
      else
        errors.add('avatar', 'wasnt succesfully uploaded')
      end
    rescue
      errors.add('avatar', 'wasnt succesfully uploaded')
    end
  end

  def nationality_country
    Country.find(nationality_country_id) if nationality_country_id
  end

  def nationality_country= country
    self.nationality_country_id = country.try(:id)
  end

  def resumes_version_ids
    resumes.map{|r| r.version_ids}.flatten
  end

  #all ids of versions with deleted
  def versioned_resumes_ids
    origin_ids = Resume.with_deleted.select(:id).where(talent_id: id).to_a
    all_versions_ids = Resume.select(:id).where("origin_id in (?)",origin_ids).to_a
  end


  # check if there is any sharing between a version of the resume and a company, which is currently valid
  def shares_with? company_type, company_id
    if company_type == "REAL"
      ResumeSharing.exists?(resume_id: resumes_version_ids, company_id: company_id, access_revoked_at: nil )
    elsif company_type == "APP"
      !VeetaButtonSharing.joins(:veeta_button_sharing_connection).where(veeta_button_sharing_connections: {client_application_id:company_id},resume_id: resumes_version_ids).empty?
    else
      ResumeSharing.exists?(resume_id: resumes_version_ids, email_application_id: company_id, access_revoked_at: nil )
    end
  end

  def currently_shared_cv company_type, company_id
    if company_type == "REAL"
      rs = ResumeSharing.where(resume_id: resumes_version_ids, company_id: company_id, access_revoked_at: nil ).order(updated_at: :desc).first
      return rs.resume if rs
  #  elsif company_type == "APP"
  #    !VeetaButtonSharing.joins(:veeta_button_sharing_connection).where(veeta_button_sharing_connections: {client_application_id:company_id},resume_id: resumes_version_ids).empty?
    else
  #    ResumeSharing.exists?(resume_id: resumes_version_ids, email_application_id: company_id, access_revoked_at: nil )
      nil
    end
    nil
  end

  def validate_image image
    begin
      if MiniMagick::Image.open(image.tempfile.path).valid?
        true
      else
        false
      end
    rescue
      false
    end
  end

  def update_avatar_metadata
    meta = nil
    if avatar && avatar.small  && !avatar.small.url.blank?
      begin
        # make hash of first half of the picture in order to be able to compare avatar_meta
        # (first half of file for avoiding comparing create/modify dates)
        #fingerprint = Digest::MD5.hexdigest(content_string[0..content_string.length/2])
        signature = MiniMagick::Image.open(avatar.small.url).signature
        meta = {}
        meta[:signature] = signature
        meta[:content_type] = avatar.content_type
        meta[:size] =  avatar.small.size
      rescue
        LogService.call 'update_avatar_metadata', {talent: self, details: "Failed to update avatar meta data",priority: :high}
      end
    end
    update_column(:avatar_meta, meta)
  end

  def recently_blocked_companies company_id
    jobapp = Jobapp.my_most_recent_jobapp_at_company(self, company_id)
    sharing_request =  CvSharingResponse.my_most_recent_sharing_response_at_company(self, company_id)
    combined = (jobapp + sharing_request)
    combined = (combined.sort_by {|obj| obj.try(:connected_at) ? obj.connected_at : obj.created_at}).reverse
    combined.empty? ? [] : combined.first.blocked_companies
  end

  def intercom_user_id
    if auth
      Digest::MD5.hexdigest(auth.id)
    end
  end

  def intercom_user_hash
    if auth
      OpenSSL::HMAC.hexdigest('sha256', ENV['INTERCOM_SECURE_MODE_SECRET_KEY'], intercom_user_id)
    end
  end

  private

    def parpare_company_path path, company_token
      path_parts = path.split '/'
      path_parts[path_parts.size-2] = company_token
      path_parts.join('/')
    end

    def unique_email
      if TalentAuth.exists?(email: email) && auth && auth.email != email
        errors.add(:server_side_validation_errors, "talent.errors.email.duplicated")
      end
    end

  class << self
    # Initialize new client based on email and options(for settings)
    # Primary used for signup
    def init email, opts = {}
      talent = Talent.new(opts.slice(:first_name, :last_name))
      talent.nationality_country_id = opts[:country_id] unless opts[:country_id].nil?
      talent.locale = I18n.locale
      talent.accept_terms opts.delete(:signed_ip)
      talent.email = email
      talent.build_setting(opts.slice(:professional_experience_level, :country_id))
      talent.address = talent.build_address
      talent.address.country_id = opts[:country_id] unless opts[:country_id].nil?
      #talent.address.save!
      talent.save!(validate: false)
      talent
    end

    # Compare talents informations
    def compare_talents o_t, c_t
      # compare talent info except listed fields
      original_talent = o_t.attributes.except('id', 'veeta_terms_accepted_at', 'veeta_terms_accepted_ip', 'new', 'ext_id','locale', 'avatar', 'avatar_meta', 'token', 'address_id', 'created_at', 'updated_at').to_a
      cloned_talent = c_t.attributes.except('id', 'veeta_terms_accepted_at', 'veeta_terms_accepted_ip', 'new', 'ext_id','locale', 'avatar', 'avatar_meta', 'token', 'address_id', 'created_at', 'updated_at').to_a
      # mechanism for compatibility (fix existing talents): https://talentsolutions.atlassian.net/browse/VEETA-2321
      if same?(original_talent, cloned_talent)
        if o_t.avatar_meta != c_t.avatar_meta
          o_t.update_avatar_metadata
          c_t.update_avatar_metadata
          o_t.reload
          c_t.reload
          return false if o_t.avatar_meta != c_t.avatar_meta
        end
      else
        return false
      end
      #byebug if !same?(original_talent, cloned_talent)
      return false if !same?(original_talent, cloned_talent)

      # compare talent address except listed fields
      original_talent_address = o_t.address.attributes.except('id', 'veeta_terms_accepted_at', 'veeta_terms_accepted_ip', 'new', 'ext_id','locale', 'avatar', 'address_id', 'created_at', 'updated_at').to_a
      cloned_talent_address = c_t.address.attributes.except('id', 'veeta_terms_accepted_at', 'veeta_terms_accepted_ip', 'new', 'ext_id','locale', 'avatar', 'address_id', 'created_at', 'updated_at').to_a
      #byebug if !same?(original_talent_address, cloned_talent_address)
      return false if !same?(original_talent_address, cloned_talent_address)

      # compare talent nationality except listed fields
      if c_t.nationality_country
        original_talent_state = o_t.nationality_country.attributes.except('id').to_a
        cloned_talent_state = c_t.nationality_country.attributes.except('id').to_a
        #byebug if !same?(original_talent_state, cloned_talent_state)
        return false if !same?(original_talent_state, cloned_talent_state)
      end
      return true
    end
    # Substract arrays and check if are the same or not
    def same? origin, clone
      (origin - clone).empty?
    end
  end
end
