# == Schema Information
#
# Table name: talent_auths
#
#  id                              :uuid             not null, primary key
#  email                           :string           default(""), not null
#  encrypted_password              :string           default(""), not null
#  reset_password_token            :string
#  reset_password_sent_at          :datetime
#  remember_created_at             :datetime
#  sign_in_count                   :integer          default(0), not null
#  current_sign_in_at              :datetime
#  last_sign_in_at                 :datetime
#  current_sign_in_ip              :string
#  last_sign_in_ip                 :string
#  confirmation_token              :string
#  confirmed_at                    :datetime
#  confirmation_sent_at            :datetime
#  unconfirmed_email               :string
#  tokens                          :text
#  provider                        :string
#  uid                             :string           default(""), not null
#  guest                           :boolean          default(FALSE)
#  talent_id                       :uuid             not null
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#  authentication_token            :string
#  authentication_token_created_at :datetime
#  xing_request_token              :string
#  xing_request_token_secret       :string
#  xing_access_token               :string
#  xing_access_token_secret        :string
#  linkedin_request_token          :string
#  linkedin_request_token_secret   :string
#  linkedin_access_token           :string
#  linkedin_access_token_secret    :string
#  failed_attempts                 :integer          default(0)
#  unlock_token                    :string
#  locked_at                       :datetime
#
# Indexes
#
#  index_talent_auths_on_authentication_token  (authentication_token)
#  index_talent_auths_on_confirmation_token    (confirmation_token) UNIQUE
#  index_talent_auths_on_created_at            (created_at)
#  index_talent_auths_on_email                 (email) UNIQUE
#  index_talent_auths_on_reset_password_token  (reset_password_token) UNIQUE
#  index_talent_auths_on_talent_id             (talent_id) UNIQUE
#  index_talent_auths_on_unlock_token          (unlock_token) UNIQUE
#

class TalentAuth < ActiveRecord::Base
  # todo FIX error from devise-token-auth, unconfirmated user could not change his email
  include DeviseTokenAuth::Concerns::User
  include Concerns::AuthType

  acts_as_token_authenticatable

  attr_accessor :resume_type, :country_id, :signed_ip, :is_guest,
                :first_name, :last_name

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  #DELAY_IT
  devise  :lockable, :database_authenticatable, :registerable, :confirmable, :async,
         :recoverable, :trackable, :validatable#, :omniauthable #, :rememberable

  belongs_to :talent

  before_create :finish
  before_create :set_talent
  before_validation :set_uid
  # after_create :trigger_email
  # before_create :skip_confirmation!

  # HACK spring followed validates if user already be created
  #validates :first_name, :last_name, format: {with: /\A([ \u00c0-\u01ffa-zA-Z'\-])+\z/i}, allow_blank: true
  validates :first_name, :last_name, presence: true, unless: :is_guest?, on: :create
  validates :resume_type, presence: true, on: :create
  validates :email, :email => true, uniqueness: { case_sensitive: false, message: 'activerecord.errors.models.talent_auth.attributes.email.taken_html' }


  alias_method :user, :talent

  has_many :client_applications, foreign_key: 'user_id'
  has_many :oauth_tokens, -> { order("authorized_at desc").includes(:client_application)}, class_name: "OauthToken", foreign_key: 'user_id'

  def delete reason
    talent.update_attribute(:deleted_at, DateTime.now)
    talent.update_attribute(:delete_reason, reason)
  end

  # Send continue apply email if user is still guest
  # ARE WE SURE WE STILL USE THIS?
  def send_continue_applying_email
  #   AuthMailer.delay.continue_applying(self) if guest?
  end

  def current_password= password
    # Doesn't do anything, it needs to be there to
    # avoid raised by Api::Talent::Auth::PasswordController
    # where we compare current password
  end

  def token_validation_response
    TalentAuthSerializer.new( self, root: false )
  end

  private
    def set_uid
      # HACK set uid with user email, this be required by devise-token-auth
      self.uid = self.email
      self.provider = 'email'
    end

    def set_talent
      opts = {professional_experience_level: resume_type,
              country_id: country_id,
              signed_ip: signed_ip,
              first_name: first_name,
              last_name: last_name}
      self.talent = Talent.init email, opts
    end

    def trigger_email
      delay_for(2.hours).send_continue_applying_email
    end

    def finish
      self.guest = is_guest?
      self.skip_confirmation_notification! if guest?
    end


  protected
    # # Override Devise::Confirmable#after_confirmation
    # def after_confirmation
    #   talent.update(email: @email)
    #   super
    # end

    def is_guest?
      is_guest == '1' || is_guest == true || is_guest == 'true'
    end
end
