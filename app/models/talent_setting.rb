# == Schema Information
#
# Table name: talent_settings
#
#  id                            :uuid             not null, primary key
#  talent_id                     :uuid             not null
#  additional_settings           :text
#  country_id                    :integer
#  professional_experience_level :integer
#  newsletter_locale             :string
#  newsletter_updates            :string
#  working_locations             :text
#  working_industries            :text
#  availability                  :string
#  job_seeker_status             :string           default("very_much")
#  prefered_employment_type      :string
#  salary_exp_min                :integer
#  salary_exp_max                :integer
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#
# Indexes
#
#  index_talent_settings_on_created_at  (created_at)
#  index_talent_settings_on_talent_id   (talent_id)
#

class TalentSetting < ActiveRecord::Base

  ## modules
  extend Enumerize

  ## settings
  enumerize :job_seeker_status, in: [:very_much, :moderately, :not_searching, :do_not_contact]
  enumerize :prefered_employment_type, in: [:full_time, :part_time, :self_employed]
  enumerize :newsletter_updates, in: %w(disabled minimum enabled), default: :enabled
  enumerize :availability, in: %w(immediately 1m 2m 3m 4-6m 6+m)

  serialize :working_industries, Array
  serialize :working_locations, Array

  ## relationships
  belongs_to :talent
  has_one :country
  include Concerns::HasCountry

  ## validations
  validates  :professional_experience_level, presence: true, on: :update

  def availability_key
    case availability
    when 'immediately'
      'immediately'
    when '1m'
      'within_month'
    when '2m'
      'within_2_months'
    when '3m'
      'within_3_months'
    when '4-6m'
      'within_4-6_months'
    when '6+m'
      'within_6+_months'
    end
  end

  def price_range_email_key
    if salary_exp_min
      if salary_exp_min < 4000
        'talent.data.salary.smaller_eml'
      elsif salary_exp_min >= 150000
        'talent.data.salary.bigger_eml'
      else
        'talent.data.salary.between_eml'
      end
    else
      'talent.data.salary.undefined'
    end
  end

  # todo why need it?
  def smart_update params
    if params[:talent_setting].keys.length == 1
      key = params[:talent_setting].keys.first.to_sym
      update_attribute(key, params[:talent_setting][key])
    else
      update(params)
    end if params[:talent_setting]
  end

  # Use talent locale settings if is no provided
  def newsletter_locale
    super || talent.locale
  end

  def resume_simplified_type
    if professional_experience_level.between?(0, 20)
      1
    elsif professional_experience_level.between?(21, 80)
      2
    elsif professional_experience_level.between?(81, 100)
      3
    end
  end
end
