# == Schema Information
#
# Table name: temp_uploads
#
#  id         :integer          not null, primary key
#  type       :string
#  file       :string
#  file_meta  :string
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_temp_uploads_on_file  (file) UNIQUE
#

module TempUpload
  class Base < ActiveRecord::Base
    self.table_name ='temp_uploads'

    serialize :file_meta

    validates :file, presence: true

    def filename
      file.path.split('/').last
    end
  end
end
