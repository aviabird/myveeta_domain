# == Schema Information
#
# Table name: temp_uploads
#
#  id         :integer          not null, primary key
#  type       :string
#  file       :string
#  file_meta  :string
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_temp_uploads_on_file  (file) UNIQUE
#

module TempUpload
  class Document < Base
    mount_uploader :file, DocumentTempUploader

    validates_integrity_of :file
    validates_processing_of :file
  end
end
