class TrapiClient < ActiveRecord::Base
  belongs_to :company

  validates :trapi_client_key, uniqueness: true

  before_create :generate_access_token

  private
  
    def generate_access_token
      begin
        self.trapi_client_key = SecureRandom.hex(32)
      end while self.class.exists?(trapi_client_key: trapi_client_key)
    end

end
