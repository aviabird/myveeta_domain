class TrapiClientInstance < ActiveRecord::Base
  belongs_to :trapi_client

  validates :trapi_client_instance_key, uniqueness: true

  before_create :generate_access_token

  def update_setting
    unless UpdateSetting.exists?(update_triggerable_id: self.id,update_triggerable_type: self.class.name, update_type: :trapi)
      UpdateSetting.create(update_triggerable_id: self.id,update_triggerable_type: self.class.name, update_type: :trapi, starting_timestamp: DateTime.new(1970,1,1), interval: '24.hours', individual: false )
    end
    UpdateSetting.find_by(update_triggerable_id: self.id,update_triggerable_type: self.class.name, update_type: :trapi)
  end

  private

    def generate_access_token
      begin
        self.trapi_client_instance_key = SecureRandom.hex(32)
      end while self.class.exists?(trapi_client_instance_key: trapi_client_instance_key)
    end

end
