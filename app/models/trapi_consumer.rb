class TrapiConsumer < ActiveRecord::Base
  belongs_to :trapi_client
  belongs_to :trapi_client_instance
  belongs_to :trapi_manufacturer


  def expired?
    current_session_token_expires_at < DateTime.now
  end

  def create_session
    self.current_session_token = SecureRandom.hex(32)
    self.current_session_token_expires_at = DateTime.now + 1.weeks
    self.last_login = DateTime.now
    self.save
  end

  def destroy_session
    self.current_session_token = nil
    self.current_session_token_expires_at = DateTime.now
    self.save
  end
end
