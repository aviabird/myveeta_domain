class TrapiManufacturer < ActiveRecord::Base

  validates :trapi_manufacturer_key, uniqueness: true

  before_create :generate_access_token

  private

  def generate_access_token
    begin
      self.trapi_manufacturer_key = SecureRandom.hex(32)
    end while self.class.exists?(trapi_manufacturer_key: trapi_manufacturer_key)
  end

end
