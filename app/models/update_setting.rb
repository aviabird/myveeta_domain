class UpdateSetting < ActiveRecord::Base


  # calculate the beginning of the current update release cycle:
  # now minus the remainder of
  ## now minus the starting timestamp of the update release lifecycle
  ## divided by the interval of one update release cycle
  def current_update_timestamp
    #eval() because interval is saved as "1.days" for example
    Time.now - ((Time.now - self.starting_timestamp) % eval(self.interval).to_i)
  end
end
