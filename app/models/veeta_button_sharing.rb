# == Schema Information
#
# Table name: veeta_button_sharings
#
#  id                                 :uuid             not null, primary key
#  resume_id                          :uuid
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#  last_synced_at                     :datetime
#  deleted_at                         :datetime
#  veeta_button_sharing_connection_id :uuid
#
# Indexes
#
#  index_veeta_button_sharings_on_created_at  (created_at)
#  index_veeta_button_sharings_on_resume_id   (resume_id)
#

class VeetaButtonSharing < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :veeta_button_sharing_connection, class_name: "VeetaButtonSharingConnection", foreign_key: "veeta_button_sharing_connection_id"
  belongs_to :resume
  has_one :origin, through: :resume, source: :origin


  scope :my, ->(talent) { joins(:veeta_button_sharing_connection).where(veeta_button_sharing_connections: {talent_id: talent.id}).order('updated_at DESC') }
  # ignore deleted resumes
  scope :my_active, ->(talent) { joins(:veeta_button_sharing_connection).where(veeta_button_sharing_connections: {talent_id: talent.id, access_revoked_at: nil}).joins(resume: :origin).where(origins_resumes: {deleted_at:nil}).order('updated_at DESC') }

  scope :sharings_of_connection, -> (vbsc) { where(veeta_button_sharing_connection_id: vbsc.id).order('updated_at DESC')}

  scope :synced_sharings_of_connection, -> (vbsc) { where(veeta_button_sharing_connection_id: vbsc.id).where.not(last_synced_at: nil).order('updated_at DESC')}


  scope :nonrevoked, -> { joins(:veeta_button_sharing_connection).where(veeta_button_sharing_connections: {access_revoked_at: nil}) }

  delegate :access_revoked_at, to: :veeta_button_sharing_connection

  def my_type
    'VeetaButtonSharing'
  end

  #expose which type of activity/jobapp this object is for activity page
  alias_method :job_type, :my_type


  ### forward attribute access
  def talent_id
    veeta_button_sharing_connection.talent.id
  end

  def client_application_id
    veeta_button_sharing_connection.client_application.id
  end

  def access_revoked_at
    veeta_button_sharing_connection.access_revoked_at
  end

  def connected_at
    veeta_button_sharing_connection.connected_at
  end
  ###

  # if last_synced is nil and the current connection was used for syncing
  # at least once, then the resume changed since last sync
  def changed_since_last_sync?
    last_synced_at.nil? && !veeta_button_sharing_connection.never_synced?
  end

  def never_synced?
      veeta_button_sharing_connection.never_synced?
  end

  def deleted_since_last_sync?
    resume && resume.deleted?
  end

  # comparison of timestamps should be enough, as updated_at for resume is only set when changed were made
  def compare
    # common comparison, orign resume exists and is compared
    # to the resume version currently shared with a third party
    if !resume.nil? && !resume.deleted?
      CloningService::ResumeQuickComparison.call(origin, resume)
    # in this case the origin resume of the resume, which is currently shared with
    # the third party, was deleted. In such a case the resume is different if
    # the deleted (i.e. empty) resume was not yet synched since the time of
    # deletion
    elsif !resume.nil? && resume.deleted?
      # veeta_button_sharing_connection holds the connection between a talent
      # and a third party. latest_synced_sharing_of_connection is the sharing
      # containing the shared resume via the associated connection
      lssoc = veeta_button_sharing_connection.latest_synced_sharing_of_connection
      origin_resume = Resume.with_deleted.find_by(id:resume.origin_id)
      # was synced && was this resume already synched to third party && was origin resume delete before the last sync to third party
      !lssoc.nil? && lssoc.resume_id == resume_id && origin_resume.deleted_at < lssoc.last_synced_at # last syned
    # in other cases (e.g. when no resume was existing when somebody used the
    # veeta button for the first time or if the resume was never synched to the
    # third party)
    else
      lssoc = veeta_button_sharing_connection.latest_synced_sharing_of_connection
      # never synced || is last synced resume to third party the resume of this sharing
      lssoc.nil? || lssoc.resume == resume
    end
  end


  def company
    veeta_button_sharing_connection.client_application
  end

  alias_method :latest?, :compare

  class << self
    def find_or_create talent, client_application, resume_id = nil
      vbsc = VeetaButtonSharingConnection.find_by(talent_id: talent.id, client_application_id:  client_application.id, access_revoked_at: nil)
      # vbsc does not exist yet -> create
      if vbsc.nil? || !vbsc.access_revoked_at.blank?
        create_date = DateTime.now
        # set wiped out date as a new connection is established an wipe out is obsolete
        vbsc_not_wiped_out_yet = VeetaButtonSharingConnection.find_by(talent_id: talent.id, client_application_id:  client_application.id, third_party_data_wiped_out_at: nil)
        vbsc_not_wiped_out_yet.update_column(:third_party_data_wiped_out_at,create_date) unless vbsc_not_wiped_out_yet.blank?
        vbsc = VeetaButtonSharingConnection.new
        vbsc.talent = talent
        vbsc.client_application = client_application
        vbsc.connected_at = create_date
        vbsc.save!
      end
      #get current vbs
      vbs = VeetaButtonSharing.sharings_of_connection(vbsc).first
      resume_id_to_share = nil
      # check if resume is latest and create latest if necessary
      if !resume_id.blank?
        origin_resume = talent.resumes.where(id: resume_id).first
        # if no latest exists yet, create one
        if origin_resume.latest.nil?
          Resume::Share.clone(origin_resume)
        end
        # resume to share is latest of the origin resume
        resume_id_to_share = origin_resume.latest.id
      end
      if vbs.nil? || vbs.resume_id != resume_id_to_share
        vbs = VeetaButtonSharing.new
        vbs.resume_id = resume_id_to_share
        vbs.veeta_button_sharing_connection = vbsc
        vbs.save!
        ass = AccessSecurityService.new
        ass.establish_sharing vbs
      end
      vbs
    end

  end

end
