# == Schema Information
#
# Table name: veeta_button_sharing_connections
#
#  id                            :uuid             not null, primary key
#  talent_id                     :uuid
#  client_application_id         :uuid
#  oauth_token_id                :integer
#  access_revoked_at             :datetime
#  connected_at                  :datetime
#  deleted_at                    :datetime
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  revoke_reason                 :string
#  third_party_data_wiped_out_at :datetime
#
# Indexes
#
#  index_veeta_button_sharing_connections_on_client_application_id  (client_application_id)
#  index_veeta_button_sharing_connections_on_oauth_token_id         (oauth_token_id)
#  index_veeta_button_sharing_connections_on_talent_id              (talent_id)
#

class VeetaButtonSharingConnection < ActiveRecord::Base
  belongs_to :talent,class_name: "Talent", foreign_key: 'talent_id'
  belongs_to :client_application
  belongs_to :oauth_token
  has_many :veeta_button_sharings, class_name: 'VeetaButtonSharing', foreign_key: 'veeta_button_sharing_connection_id'

  scope :my, ->(talent) { where(talent_id: talent.id).order('updated_at DESC') }
  scope :my_active, ->(talent) { where(talent_id: talent.id, third_party_data_wiped_out_at: nil).order('updated_at DESC') }
  scope :my_current_connections, ->( resume) { joins(:veeta_button_sharings).where(veeta_button_sharings: {resume: resume}).where(access_revoked_at: nil).order('updated_at DESC').uniq }
  scope :my_connections_via_resume_versions, ->( resume_version_ids) { joins(:veeta_button_sharings).where("resume_id in (?)",resume_version_ids).where(access_revoked_at: nil).order('updated_at DESC').uniq }


  def latest_sharing_of_connection
    VeetaButtonSharing.sharings_of_connection(self).first
  end

  def latest_synced_sharing_of_connection
    VeetaButtonSharing.synced_sharings_of_connection(self).first
  end

  def initial_sharing_of_connection
    VeetaButtonSharing.sharings_of_connection(self).last
  end


  def never_synced?
    veeta_button_sharings.where.not(last_synced_at: nil).count == 0
  end


  # Revoke access by flagging as revoked
  def revoke revoke_reason = nil
    update_columns(access_revoked_at: DateTime.now, revoke_reason: revoke_reason)
    AccessSecurityService.new.revoke_access veeta_button_sharings
  end

  # User wiped data on third party side.
  def third_party_data_wiped_out
    update_columns(third_party_data_wiped_out_at: DateTime.now)
  end

  class << self

    # get the initially shared resume for each connection
    def initial_sharing_of_connections talent
      my_connections = VeetaButtonSharingConnection.my(talent)
      initial_sharings = []
      my_connections.each do |connection|
        initial_sharings << connection.initial_sharing_of_connection
      end
      initial_sharings
    end

    # get the latest sharings of active connections
    def current_connections talent
      my_connections = VeetaButtonSharingConnection.my_active(talent)
      current_connections = []
      my_connections.each do |connection|
        current_connections << connection.latest_sharing_of_connection
      end
      current_connections
    end
  end

end
