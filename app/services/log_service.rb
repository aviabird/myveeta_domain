class LogService
  attr_reader :key, :user, :email, :source, :source_ip, :source_referrer_url, :priority, :resource, :status, :details, :request, :limit_log, :notify_im

  # Create new instance, fetch message as first
  #   args[:user] - user who invoked acction, current_user by default
  #   args[:email] - email of given user, current_user email by default
  #   args[:source] - default is current_user class, Anonymous when is current_user not provided, or System
  #   args[:priority] - specify priority, :low by default
  #   args[:status] - log statis, if is information or error..
  #   args[:details] - provide more details if you want
  #   args[:data] - inspect of resource by default
  #   args[:resume] - by default resume related to resource
  #   args[:jobapp] - by default jobapp related to resource
  #   args[:company] - by default company related to resource
  #   args[:talent] - by default talent related to resource
  #   args[:recruiter] - by default recruiter related to resource
  #   args[:admin] - by default admin related to resource
  def initialize key, args
    @key = key
    @user = args[:user]
    @email = args[:email]
    @source = args[:source]
    @source_ip = args[:source_ip]
    @source_referrer_url = args[:source_referrer_url]
    @priority = args.fetch(:priority, :low)
    @resource = args[:resource]
    @status = args.fetch(:status, :information)
    @details = args[:details]
    @data = args[:data]
    @resume = args[:resume]
    @jobapp = args[:jobapp]
    @company = args[:company]
    @talent = args[:talent]
    @recruiter = args[:recruiter]
    @admin = args[:admin]
    @request = args[:request]
    @limit_log = args[:limit_log]
    @notify_im = args[:notify_im]
    controller_infos
  end

  # Invoke save and return self
  def call
    save
    self
  end

  # Call service directly
  def self.call key, args = {}
    new(key, args).call #unless Rails.env.development?
  end

  # Get source as user class, anonymous or system if not provided
  def source
    @source ||= if !@user.nil?
      @user.class.name
    elsif @email
      'Anonymous'
    else
      'System'
    end
  end

  # Get type of resource
  def resource_type
    @resource.class.name if @resource
  end

  # Get resource id
  def resource_id
    @resource.id if @resource && @resource.respond_to?(:id)
  end

  # Get resume id
  def resume_id
    @resume ? @resume.id : ( @resource.respond_to?(:resume) ? @resource.resume.try(:id) : nil )
  end

  # Get jobapp id
  def jobapp_id
    @jobapp ? @jobapp.id : ( @resource.respond_to?(:jobapp) ? @resource.jobapp.try(:id) : (@resource && @resource.kind_of?(Jobapp) ? @resource.id : nil) )
  end

  # Get company id
  def company_id
    @company ? @company.id : ( @resource.respond_to?(:company) ? @resource.company.try(:id) : nil )
  end

  # Get talent id
  def talent_id
    @talent ? @talent.id : ( @resource.respond_to?(:talent) ? @resource.talent.try(:id) : nil )
  end

  # Get recruiter id
  def recruiter_id
    @recruiter ? @recruiter.id : ( @resource.respond_to?(:recruiter) ? @resource.recruiter.try(:id) : nil )
  end

  # Get admin id
  def admin_id
    @admin ? @admin.id : ( @resource.respond_to?(:admin) ? @resource.admin.try(:id) : nil )
  end

  # Get image of resource
  def data
    @data ? @data : ( @resource ? @resource.inspect : nil )
  end

  # Get user id
  def user_id
    @user ? @user.id : nil
  end

  # make log entry only if it was not logged in last x minutes
  def limit_log
    @limit_log ? @limit_log : false
  end

  # notify via instant messanger
  def notify_im
    @notify_im ? @notify_im : false
  end

  private
    # Assign everything we can from controller
    def controller_infos
      if PublicActivity.get_controller
        @user = PublicActivity.get_controller.current_user unless @user
        @email = PublicActivity.get_controller.current_email unless @email
        @source_ip = PublicActivity.get_controller.request.remote_ip unless @source_ip
        @source_referrer_url = PublicActivity.get_controller.request.env["HTTP_REFERER"] unless @source_referrer_url
      elsif @request
        @source_ip = client_ip(@request) unless @source_ip
        @source_referrer_url = @request.env["HTTP_REFERER"] unless @source_referrer_url
      end
    end

    # Store data locally
    def save
      #  ResumeSharing.exists?(company_id: company.id, access_revoked_at: nil, resume_id: Resume.all_versions_of_origins(user.resumes.with_deleted).pluck(:id).to_a)
      if !limit_log || !Log.where('created_at > ?', DateTime.now-5.minutes).exists?(source: source.to_s.truncate(255), source_user_email: email.to_s.truncate(255), key: key.to_s.truncate(255), priority: priority.to_s.truncate(255), source_ip: source_ip.to_s.truncate(255))
        entry = Log.create(
          source: source.to_s.truncate(255),
          source_user_id: user_id,
          source_user_email: email.to_s.truncate(255),
          source_ip: source_ip.to_s.truncate(255),
          source_referrer_url: source_referrer_url.to_s.truncate(255),
          priority: priority.to_s.truncate(255),
          key: key.to_s.truncate(255),
          resource_type: resource_type.to_s.truncate(255),
          resource_id: resource_id.to_s.truncate(255),
          status: status.to_s.truncate(255),
          details: details.to_s.truncate(255),
          data: data.to_s,
          resume_id: resume_id,
          jobapp_id: jobapp_id,
          company_id: company_id,
          talent_id: talent_id,
          recruiter_id: recruiter_id,
          admin_id: admin_id )
        if notify_im
          notifier = Slack::Notifier.new ENV['SLACK_NOTIFIER_CHANNEL']
          notifier.ping "*Source IP:* #{entry.source_ip}, *Key:* #{entry.key}, *Priority:* #{entry.priority}, *User-Email:* #{entry.source_user_email}, *Details:* #{entry.details}, *LogEntryId:* #{entry.id}", username: "myVeeta #{Rails.env} Log Bot"
        end
        if defined?(IntercomService) || (Object.const_get(:IntercomService).is_a?(Class) rescue false)
          IntercomService.call(entry)
        end
      end
    end

    def client_ip request
      ip = ""
      if request.headers['HTTP_X_FORWARDED_FOR']
        ips = request.headers['HTTP_X_FORWARDED_FOR'].split(",")
        if ips
          ip = ips[0].strip
        end
      end
      if ip.blank?
        ip = request.remote_ip
      end
      ip
    end
end
