# encoding: utf-8

class AvatarTempUploader < BaseUploader

  def store_dir
      Rails.root.join 'tmp/uploads/temp_upload'
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end
end
