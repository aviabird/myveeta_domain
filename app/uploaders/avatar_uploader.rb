# encoding: utf-8

class AvatarUploader < AvatarTempUploader

  include CarrierWave::MiniMagick
  include Sprockets::Rails::Helper

  SALT = 'fcka4Dqd0caqf9'

  after :cache, :save_token

  process :auto_orient

  def store_dir
    "dl/#{token_path}"
  end
  version :thumb do
    process :resize_to_fit  => [50, 50]
  end

  version :small do
    process :resize_to_fit  => [150, 150]
  end

  version :medium  do
    process :resize_to_fit  => [380, 380]
  end

  version :large  do
    process :resize_to_fit  => [600, 600]
  end

  def auto_orient
     manipulate! do |img|
       img.auto_orient
       img
     end
   end

  def refresh_token
    model.update_column(:token, token(true))
    model.token ||= token
  end

  def refresh_full_token
    @token = nil
    model.token = nil
    model.update_column(:token, token)
    model.token ||= token
  end

  protected
    def save_file_meta(file)
      content = model.send(mounted_as)
      content_string = content.small.read.to_s
      #byebug
      # make hash of first half of the picture in order to be able to compare avatar_meta
      # (first half of file for avoiding comparing create/modify dates)
      fingerprint = Digest::MD5.hexdigest(content_string[0..content_string.length/2])
      model.send("#{mounted_as}_meta=", {
          original_filename: fingerprint,
          content_type: file.content_type,
          size: content.small.size,
      })
    end

    def url_safe string
      string.gsub(/[_\/+-]/, '0').downcase
    end

    # Use token from model or generate new one when doesn't exist
    def token refresh = false
      if refresh
        @token = "#{@token[0, 41]}#{generate_token[41, 8]}"
      else
        @token ||= model.token? ? model.token : generate_token[0, 49]
        unless model.token?
          model.token =  @token
          model.update_column(:token, @token) unless model.new_record?
        end
        @token
      end
    end

    # Split token into path
    def token_path
      token[0, 1].downcase + '/' + token[1, 8] + '/' + token[9, 32] + '/' + token[41, 8]
    end

    # Current date
    # to_s is important to get the right format
    def date
      Date.today.to_s
    end

    # Generate digest version of date and strip first 8 characters
    def date_hash
      Digest::SHA2.base64digest(date + SALT)[0,8]
    end

    # Generate random url safe string
    def pepper
      url_safe SecureRandom.urlsafe_base64(8)
    end

    # Save current file token
    def save_token file
      model.token ||= token
    end

    # Generate unique token excluding pepper
    def generate_token
      loop do
        token = url_safe(model.class.name[0] + date_hash + SecureRandom.urlsafe_base64(32))
        break token unless model.class.exists?(['token LIKE ?', "#{token}%"])
      end
    end
end
