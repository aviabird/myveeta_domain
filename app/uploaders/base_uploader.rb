# encoding: utf-8

class BaseUploader < CarrierWave::Uploader::Base

  include CarrierWave::MimeTypes

  before :cache, :save_file_meta

  storage :webdav

  def store_dir
    "uploads/fallback/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def filename
    begin
      "#{secure_token}.#{file.extension}" if original_filename.present?
    rescue
    end
  end

  def extension_white_list
    []
  end

  protected
  def secure_token
    var = :"@#{mounted_as}_secure_token"
    model.instance_variable_get(var) or model.instance_variable_set(var, SecureRandom.uuid)
  end

  def save_file_meta(file)
    model.send("#{mounted_as}_meta=", {
        original_filename: file.original_filename,
        content_type: file.content_type,
        size: file.size,
    })
  end
end
