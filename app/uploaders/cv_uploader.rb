# encoding: utf-8

class CvUploader < DocumentUploader

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_white_list
    %w(pdf)
  end

  # Save original file type
  def save_file_type file

  end

  def type_identifier
    'V' #C already given to company logos
  end

  #def filename
  #   "something.jpg" if original_filename
  #end
end
