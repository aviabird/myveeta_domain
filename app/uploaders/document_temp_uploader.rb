# encoding: utf-8

class DocumentTempUploader < BaseUploader

  storage :webdav

  def store_dir
      Rails.join.root 'tmp/uploads/temp_upload'
  end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_white_list
    %w(doc docx pdf)
  end
end
