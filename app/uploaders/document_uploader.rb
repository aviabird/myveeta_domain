# encoding: utf-8

class DocumentUploader < CarrierWave::Uploader::Base

  SALT = 'fcka4Dqd0caqf9'

  after :cache, :save_original_filename
  after :cache, :save_file_type
  after :cache, :save_token

  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  # include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  storage :webdav
  # storage :fog

  root Rails.root.join('public')
  asset_host ENV['HOST']

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "dl/#{token_path}"
  end

  def type_identifier
    'D'
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  # version :thumb do
  #   process :resize_to_fit => [50, 50]
  # end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_white_list
    %w(pdf doc docx jpg jpeg png txt tif tiff bmp xls xlsx rtf odf gif ppt pptx pages numbers key)
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end

  def url_safe string
    string.gsub(/[_\/+-]/, '0').downcase
  end

  def refresh_token
    model.update_column(:token, token(true))
  end

  protected
    # Use token from model or generate new one when doesn't exist
    def token refresh = false
      if refresh
        @token = "#{@token[0, 41]}#{generate_token[41, 8]}#{pepper}"
      else
        @token ||= model.token? ? model.token : generate_token + pepper
      end
    end

    # Split token into path
    def token_path
     token[0, 1].downcase + '/' + token[1, 8] + '/' + token[9, 32] + '/' + token[41, 8]
    end

    # Current date
    # to_s is important to get the right format
    def date
      Date.today.to_s
    end

    # Generate digest version of date and strip first 8 characters
    def date_hash
      Digest::SHA2.base64digest(date + SALT)[0,8]
    end

    # Generate random url safe string
    def pepper
      url_safe SecureRandom.urlsafe_base64(8)
    end

    # Save original filename
    def save_original_filename file
      model.name ||= file.original_filename if file.respond_to?(:original_filename)
    end

    # Save original file type
    def save_file_type file
      model.file_type ||= file.extension if file.respond_to?(:extension)
    end

    # Save current file token
    def save_token file
      model.token ||= token
    end

    # Generate unique token excluding pepper
    def generate_token
      loop do
        token = url_safe(type_identifier + date_hash + SecureRandom.urlsafe_base64(32))
        break token unless model.class.exists?(['token LIKE ?', "#{token}%"])
      end
    end
end
