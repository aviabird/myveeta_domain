
### CarrierWave.configure do |config|
###   # These permissions will make dir and files available only to the user running
###   # the servers
###   config.permissions = 0600
###   config.directory_permissions = 0700
###   config.storage = :file
###   # This avoids uploaded files from saving to public/ and so
###   # they will not be available for public (non-authenticated) downloading
###   # config.root = Rails.root.join('private')
### end
CarrierWave.configure do |config|
  config.storage = :webdav
  config.webdav_server = ENV['WEBDAV_READ_URL'] # Your WebDAV url.
  config.webdav_write_server = ENV['WEBDAV_WRITE_URL'] # This is an optional attribute. It can save on one server and read from another server. (Contributed by @eychu. Thanks)
  #config.webdav_autocreates_dirs = true # if automatic directory creation is enabled on the server (disables explicit directory creation)
  config.webdav_username = ENV['WEBDAV_USER']
  config.webdav_password = ENV['WEBDAV_PASSWORD']
end
