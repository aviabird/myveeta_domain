class CreateTrapiManufacturers < ActiveRecord::Migration
  def change
    create_table :trapi_manufacturers, id: :uuid do |t|
      t.string :trapi_manufacturer_key
      t.string :name
      t.text :description
      t.text :contact_info
      t.string :manufacturer_type
      t.timestamps null: false
    end
    add_index :trapi_manufacturers, :trapi_manufacturer_key, :unique => true
    add_index :trapi_manufacturers, :manufacturer_type
  end
end
