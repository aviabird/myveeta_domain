class CreateTrapiClients < ActiveRecord::Migration
  def change
    create_table :trapi_clients, id: :uuid do |t|
      t.string :trapi_client_key
      t.uuid :company_id, null: false
      t.string :name
      t.text :description
      t.text :contact_info
      t.timestamps null: false
    end
    add_index :trapi_clients, :trapi_client_key, :unique => true
    add_index :trapi_clients, :company_id
  end
end
