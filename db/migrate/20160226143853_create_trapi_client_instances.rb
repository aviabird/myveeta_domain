class CreateTrapiClientInstances < ActiveRecord::Migration
  def change
    create_table :trapi_client_instances, id: :uuid do |t|
      t.string :trapi_client_instance_key
      t.uuid :trapi_client_id, null: false
      t.string :name
      t.text :description
      t.timestamps null: false
    end
    add_index :trapi_client_instances, :trapi_client_instance_key, :unique => true
    add_index :trapi_client_instances, :trapi_client_id
  end
end
