class CreateTrapiConsumers < ActiveRecord::Migration
  def change
    create_table :trapi_consumers, id: :uuid do |t|
      t.uuid :trapi_manufacturer_id
      t.uuid :trapi_client_id
      t.uuid :trapi_client_instance_id
      t.datetime :last_login
      t.string :current_session_token
      t.datetime :current_session_token_expires_at
      t.string :created_by
      t.timestamps null: false
    end
    add_index :trapi_consumers, :current_session_token
    add_index :trapi_consumers, :trapi_manufacturer_id
    add_index :trapi_consumers, :trapi_client_id
    add_index :trapi_consumers, :trapi_client_instance_id
  end
end
