class AddReasonAndSubmissionDateToJobapp < ActiveRecord::Migration
  def change
    add_column :jobapps, :revoke_reason, :string
    add_column :jobapps, :submitted_at, :datetime
    add_column :cv_sharing_responses, :revoke_reason, :string
    add_column :cv_sharing_responses, :submitted_at, :datetime

    execute "UPDATE jobapps SET submitted_at=created_at WHERE submitted_at IS NULL;"
    execute "UPDATE cv_sharing_responses SET submitted_at=created_at WHERE submitted_at IS NULL;" 


  end
end
