class AddSequentialIdAndHashIdToCompanyTalentInfo < ActiveRecord::Migration
  def change
    drop_table :company_talent_infos
    create_table :company_talent_infos, id: :uuid do |t|
      t.uuid :company_id
      t.uuid :talent_id
      t.integer :rating
      t.text :note
      t.string :category
      t.integer :sequential_id, null: false
      t.string :hashed_talent_id
      t.string :hashed_talent_id_salt
      t.timestamps null: false
    end

    add_index :company_talent_infos, :hashed_talent_id
    add_index :company_talent_infos, :sequential_id

    add_index :company_talent_infos, [:sequential_id, :company_id], unique: true

    add_index :company_talent_infos, :company_id
    add_index :company_talent_infos, :talent_id

  end
end
