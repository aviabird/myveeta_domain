class CreateUpdateSettings < ActiveRecord::Migration
  def change
    create_table :update_settings, id: :uuid do |t|
      t.uuid :update_triggerable_id
      t.string :update_triggerable_type
      t.string :update_type #email, trapi etc.
      t.datetime :starting_timestamp
      t.string :interval
      t.datetime :last_executed_at
      t.string :lookup_token
      t.boolean :individual
      t.timestamps null: false
    end

    add_index :update_settings, [:update_triggerable_id,:update_triggerable_type,:update_type,:lookup_token], unique: true, name: 'id_type_update_type_lookup_token'
    add_index :update_settings, [:update_triggerable_id,:update_triggerable_type,:update_type], name: 'id_type_update_type'
    add_index :update_settings, :lookup_token

  end
end
