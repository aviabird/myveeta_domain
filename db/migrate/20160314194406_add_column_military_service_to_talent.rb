class AddColumnMilitaryServiceToTalent < ActiveRecord::Migration
  def change
    add_column :talents, :military_service, :string
  end
end
