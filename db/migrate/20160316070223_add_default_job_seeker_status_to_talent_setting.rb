class AddDefaultJobSeekerStatusToTalentSetting < ActiveRecord::Migration
  def change
    change_column :talent_settings, :job_seeker_status, :string, :default => 'very_much'
  end
end
