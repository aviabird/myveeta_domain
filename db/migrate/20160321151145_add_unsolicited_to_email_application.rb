class AddUnsolicitedToEmailApplication < ActiveRecord::Migration
  def change
    add_column :email_applications, :unsolicited, :boolean
  end
end
