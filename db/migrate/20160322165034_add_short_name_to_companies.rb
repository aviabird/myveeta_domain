class AddShortNameToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :short_name, :string
    add_index :companies, :short_name, unique: true
    Company.all.each do |c|
      short_name = c.name.gsub(" ","")
      short_name = short_name[0, [short_name.length,3].min]
      c.short_name = short_name.upcase
      c.save
    end
  end
end
