class ChangeModeratelyInterestedToNotInterestedInTalentSettings < ActiveRecord::Migration
  def up
    TalentSetting.where(job_seeker_status: 'moderately').each do |ts|
      ts.update(job_seeker_status: 'not_searching')
    end
  end

  def down
    puts "Job seeker status cannot be moderately interested now its removed"
  end

end
