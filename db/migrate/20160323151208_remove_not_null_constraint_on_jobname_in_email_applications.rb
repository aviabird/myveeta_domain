class RemoveNotNullConstraintOnJobnameInEmailApplications < ActiveRecord::Migration
  def change
    change_column :email_applications, :jobname, :string, :null => true
  end
end
