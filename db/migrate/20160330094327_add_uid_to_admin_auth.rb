class AddUidToAdminAuth < ActiveRecord::Migration
  def change
    add_column :admin_auths, :uid, :string
  end
end
