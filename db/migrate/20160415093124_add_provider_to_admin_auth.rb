class AddProviderToAdminAuth < ActiveRecord::Migration
  def change
    add_column :admin_auths, :provider, :string
    add_column :admin_auths, :tokens, :string
  end
end
