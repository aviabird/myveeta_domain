class AddUnsolicitedJobToJob < ActiveRecord::Migration
  def change
    add_column :jobs, :unsolicited_job, :boolean, :default => false
  end
end
