class RemoveNotNullConstraintOnNameForJob < ActiveRecord::Migration
  def change
    change_column :jobs, :name, :string, :null => true
  end
end
