# requires all dependencies
require 'rubygems'
require 'acts-as-taggable-on'
require 'enumerize'
require 'paranoia'
require 'iso_country_codes'
require 'language_list'
require 'amoeba'
require 'chewy'
#require 'elasticsearch-rails'
#require 'elasticsearch-model'
require 'public_activity'
require 'date_validator'
require 'activevalidators'
require 'carrierwave'
require 'mini_magick'
require 'aasm'
require 'sequenced'
require 'slack-notifier'
require 'devise'
require 'simple_token_authentication'
require 'devise_token_auth'
require 'devise-async'

#Gem.loaded_specs['myveeta_domain'].dependencies.each do |d|
# require d.name
#end

require "myveeta_domain/engine"

module MyveetaDomain
end
