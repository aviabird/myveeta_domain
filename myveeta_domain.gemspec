$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "myveeta_domain/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "myveeta_domain"
  s.version     = MyveetaDomain::VERSION
  s.authors     = ["Günther Pfeffer"]
  s.email       = ["pfeffer@guentr.com"]
  s.homepage    = "https://www.myveeta.com"
  s.summary     = 'All models of the myVeeta domain'
  s.description = 'All models of the myVeeta domain'
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.2.5"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency "bundler", "~> 1.11"
  s.add_development_dependency "rake", "~> 10.0"
  s.add_development_dependency "minitest", "~> 5.0"
  s.add_dependency "paranoia", "~> 2.0"
  s.add_dependency "enumerize"
  s.add_dependency 'iso_country_codes'
  s.add_dependency 'language_list','=1.2.0'
  s.add_dependency "acts-as-taggable-on"
  s.add_dependency 'amoeba'
  s.add_dependency 'chewy'
  s.add_dependency 'elasticsearch-model'
  s.add_dependency 'elasticsearch-rails'
  s.add_dependency 'public_activity'
  s.add_dependency 'date_validator'
  s.add_dependency 'activevalidators'
  s.add_dependency 'carrierwave'
  s.add_dependency 'mini_magick'
  s.add_dependency 'aasm'
  s.add_dependency 'sequenced'
  s.add_dependency 'slack-notifier'
  s.add_dependency 'devise'
  s.add_dependency 'simple_token_authentication'
  s.add_dependency 'devise_token_auth'
  s.add_dependency 'devise-async'
end
